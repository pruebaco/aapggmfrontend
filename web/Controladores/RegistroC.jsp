<%-- 
    Document   : EstudianteM
    Created on : 29/11/2019, 15:00:48
    Author     : Angel
--%>

<%@page import="ggm.comun.periodo"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    try {
        String opcion = request.getParameter("opcion"); //Opciones
        String nombreU = request.getParameter("nombreU");
        String apellidoU = request.getParameter("apellidoU");
        String perioAcaU = request.getParameter("perioAcaU");
        String cedulaU = request.getParameter("cedulaU");
        String edadU = request.getParameter("edadU");
        String correoU = request.getParameter("correoU");
        String contrasenaU = request.getParameter("contrasenaU");
        String tipoU = request.getParameter("tipoU");
        String telefonoU = request.getParameter("telefonoU");
        String celular1U = request.getParameter("celular1U");
        String celular2U = request.getParameter("celular2U");
        System.out.println("Datos del Usuario: " + nombreU + " " + apellidoU + " " + perioAcaU + " " + cedulaU + " " + edadU + " " + correoU + " " + contrasenaU + " " + tipoU + " " + telefonoU + " " + celular1U + " " + celular2U);
        if (opcion.equals("registrarUsuarios")) {
            System.out.println("Listo para enviar datos ");
            periodo oPeriodo = new periodo();
            usuario oUsuario = new usuario();
            Gson oGson = new Gson();
            
            oUsuario.setStrNombre(nombreU);
            oUsuario.setStrApellido(apellidoU);
            oUsuario.setStrCedula(cedulaU);
            oUsuario.setIntEdad(Integer.parseInt(edadU));
            oUsuario.setStrCorreo(correoU);
            oUsuario.setStrContrasena(contrasenaU);
            oUsuario.setStrTipo(tipoU);
            oUsuario.setStrTelefono(telefonoU);
            oUsuario.setStrCelular1(celular1U);
            oUsuario.setStrCelular2(celular2U);
            
            oPeriodo.setIntIdPeriodo(Integer.parseInt(perioAcaU));
            String dPeriodo = oGson.toJson(oPeriodo);
            
            String dUsuario = oGson.toJson(oUsuario);
            response.sendRedirect("../Modelos/RegistroM.jsp?dUsuario=" + dUsuario + "&dPeriodo=" + dPeriodo + "&opcion=" + opcion);
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  MateriasM " + e.toString());
    }
%>
