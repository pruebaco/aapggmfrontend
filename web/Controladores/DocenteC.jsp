<%-- 
    Document   : controladorIngresarParalelo
    Created on : 13/11/2019, 17:17:22
    Author     : Angel
--%>

<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="java.lang.ProcessBuilder.Redirect.Type"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="ggm.comun.materia"%>
<%@page import="ggm.comun.periodo"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.util.List"%>
<%@page import="ggm.comun.paralelo"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.docente"%>
<%@page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        String opcion = request.getParameter("opcion"); //Opcion para ingresar materia

        String datos = request.getParameter("datos"); //Datos obtenidos del LoginC

        //Objeto principal del docente datosD
        String datosDocente = request.getParameter("datosD"); //Datos obtenidos de DocenteM del web service
        paralelo oparalelo = new paralelo();
        Gson oGson = new Gson();

// Recibe datos del formulario para ingresar datos del paralelo 
        String paralelo = request.getParameter("paralelo");
        String descripcion = request.getParameter("descripcion");

//Obtener el string de la lista de estudiantes
        String campo = request.getParameter("campo");
        System.out.println("Lista importada del archivo csv: " + campo);

//    Obtiene el data del nuevo periodo académico
        periodo operiodoAca = new periodo();
        String periodoAcade = request.getParameter("periodoAcade");
        System.out.println("Nuevo Periodo " + periodoAcade);

//    Obtener los nuevos datos para el ingreso de la nueva materia
        materia oMateria = new materia();
        String nombreMateria = request.getParameter("nombreMateria");
        String descripcionMat = request.getParameter("descripcionMat");
        String paraleloMat = request.getParameter("paraleloMat");

//        Con el tipo de Usuario se redirigir al menu principal del docente
        if (datos != null) {
            response.sendRedirect("../Modelos/DocenteM.jsp?datos=" + datos);
            System.out.println("Datos desde el login " + datos);

//        } else if (!datosDocente.equals("")) {
        } else if (datosDocente != null) {
            //Crea la sesion del docente 
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("datosDocente", datosDocente);

            response.sendRedirect("../Vistas/PanelDocenteV.jsp");
            System.out.println("Datos enviados a la vista " + datosDocente);

        } else if ((paralelo != null) && (descripcion != null)) {
//          Ingrear un paralelo nuevo 
            oparalelo.setStrParalelo(paralelo);
            oparalelo.setStrDescripcion(descripcion);
            String dparalelo = oGson.toJson(oparalelo);

//             ArrayList<ListaE> arrayDeJson = gson.fromJson(jsonArray, listType);
//            System.out.println("Lista convertida: " + JSON);

            response.sendRedirect("../Modelos/ParaleloM.jsp?dparalelo=" + dparalelo + "&dLista=" + campo);
        } else if (opcion.equals("ingresarPeriodoAca")) {
            operiodoAca.setStrPeriodoAcademico(periodoAcade);
            String dperiodo = oGson.toJson(operiodoAca);
            response.sendRedirect("../Modelos/PeriodoAcaM.jsp?dperiodo=" + dperiodo+ "&opcion=" + opcion);
            
        } else if (opcion.equals("ingresarMateria")) {
            oMateria.setStrNombreMAteria(nombreMateria);
            oMateria.setStrDescripcionMateria(descripcionMat);
            String dMateria = oGson.toJson(oMateria);
            response.sendRedirect("../Modelos/MateriasM.jsp?dMateria=" + dMateria + "&paraleloMat=" + paraleloMat + "&opcion=" + opcion);
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  DocenteC");

    }
%>