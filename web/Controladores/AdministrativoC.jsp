<%-- 
    Document   : controladorIngresarParalelo
    Created on : 13/11/2019, 17:17:22
    Author     : Angel
--%>

<%@page import="ggm.comun.administrativo"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {
        String opcion = request.getParameter("opcion");
        String datos = request.getParameter("datos"); //Datos obtenidos del LoginC
        String dataAdm = request.getParameter("dataAdm"); //Datos obtenidos del administrativoM

        String nombreA = request.getParameter("nombreA");
        String apellidoA = request.getParameter("apellidoA");
        String cedulaA = request.getParameter("cedulaA");
        String edadA = request.getParameter("edadA");
        String correoA = request.getParameter("correoA");
        String telefonoA = request.getParameter("telefonoA");
        String celular1A = request.getParameter("celular1A");
        String celular2A = request.getParameter("celular2A");
        String cargoA = request.getParameter("cargoA");
        String titulo1A = request.getParameter("titulo1A");
        String titulo2A = request.getParameter("titulo2A");

        if (datos != null) {
            response.sendRedirect("../Modelos/AdministrativoM.jsp?datos=" + datos);
        } else if (dataAdm != null) {
            //Crea la sesion del Administrador 
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("datosAdministrador", dataAdm);

            response.sendRedirect("../Vistas/PanelAdmonistradorV.jsp");
        } else if (opcion.equals("mostrarPerfil")) {
            response.sendRedirect("../Vistas/PerfilAdministrador.jsp");
        } else if (opcion.equals("modificarDatos")) {
            administrativo oAdm = new administrativo();
            Gson oGson = new Gson();
            oAdm.setStrNombre(nombreA);
            oAdm.setStrApellido(apellidoA);
            oAdm.setStrCedula(cedulaA);
            oAdm.setIntEdad(Integer.parseInt(edadA));
            oAdm.setStrCorreo(correoA);
            oAdm.setStrTelefono(telefonoA);
            oAdm.setStrCelular1(celular1A);
            oAdm.setStrCelular2(celular2A);
            oAdm.setStrCargo(cargoA);
            oAdm.setStrTitulo1(titulo1A);
            oAdm.setStrTitulo2(titulo2A);
            String dataModificarAdm = oGson.toJson(oAdm);
            response.sendRedirect("../Modelos/AdministrativoM.jsp?dataModificarAdm=" + dataModificarAdm + "&opcion=" + opcion);
        }else if(opcion.equals("datosActualizados")){
            
            //    Datos modificados
            String datosADministrador = request.getParameter("datosAct");
            System.out.println("Datos actualizados: " + datosADministrador);
            
            //Crea la sesion del Administrador 
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("datosAdministrador", datosADministrador);
            response.sendRedirect("../Vistas/PerfilAdministrador.jsp");
            
        
        }
    } catch (Exception e) {

    }
%>