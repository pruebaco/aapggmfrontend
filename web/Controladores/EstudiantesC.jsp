<%-- 
    Document   : EstudianteM
    Created on : 29/11/2019, 15:00:48
    Author     : Angel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    try {
        String paraleloMat = request.getParameter("paraleloMat");
        String materiaEst = request.getParameter("materiaEst");
        String quimestreEst = request.getParameter("quimestreEst");
        String parcialEst = request.getParameter("parcialEst");
        
        String idEstudiante = request.getParameter("idEstudiante");
        String idEvaluacion = request.getParameter("idEvaluacion");
        
        String opcion = request.getParameter("opcion"); //Opciones
        String paraleloEstudiante = request.getParameter("paraleloEstudiante");
        String quimesteEstudiante = request.getParameter("quimesteEstudiante");
        String parcialEstudiante = request.getParameter("parcialEstudiante");
        if (opcion.equals("listarParalelosDoc")) {
            response.sendRedirect("../Modelos/EstudianteM.jsp?opcion=" + opcion);
        } else if (opcion.equals("guardarNotas")) {
            String notasEstudiante = request.getParameter("notasEstudiante");
            response.sendRedirect("../Modelos/EstudianteM.jsp?opcion=" + opcion + "&notasEstudiante=" + notasEstudiante + "&paraleloMat=" + paraleloMat + "&materiaEst=" + materiaEst + "&quimestreEst=" + quimestreEst + "&parcialEst=" + parcialEst + "&idEstudiante=" + idEstudiante + "&idEvaluacion=" + idEvaluacion);
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  MateriasM " + e.toString());
    }
%>
