<%-- 
    Document   : DocenteOpC
    Created on : 19/11/2019, 11:49:23
    Author     : Angel
--%>

<%@page import="ggm.comun.representante"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String opcion = request.getParameter("opcion");
    String nombreR = request.getParameter("nombreR");
    String apellidoR = request.getParameter("apellidoR");
    String cedulaR = request.getParameter("cedulaR");
    String edadR = request.getParameter("edadR");
    String correoR = request.getParameter("correoR");
    String telefonoR = request.getParameter("telefonoR");
    String celular1R = request.getParameter("celular1R");
    String celular2R = request.getParameter("celular2R");
    String nivelAcadR = request.getParameter("nivelAcadR");
    String direccionR = request.getParameter("direccionR");

    try {
        //        Mostrar el perfil del docente
        if (opcion.equals("mostrarPerfil")) {
            response.sendRedirect("../Vistas/PerfilRepresentante.jsp");
        } else if (opcion.equals("modificarDatos")) {
            representante oRepre = new representante();
            Gson oGson = new Gson();

            oRepre.setStrNombre(nombreR);
            oRepre.setStrApellido(apellidoR);
            oRepre.setStrCedula(cedulaR);
            oRepre.setIntEdad(Integer.parseInt(edadR));
            oRepre.setStrCorreo(correoR);
            oRepre.setStrTelefono(telefonoR);
            oRepre.setStrCelular1(celular1R);
            oRepre.setStrCelular2(celular2R);
            oRepre.setStrNivelAca(nivelAcadR);
            oRepre.setStrDireccion(direccionR);
            String dataModificarR = oGson.toJson(oRepre);
            response.sendRedirect("../Modelos/RepresentanteM.jsp?dataModificarR=" + dataModificarR + "&opcion=" + opcion);
        }
        if (opcion.equals("datosActualizados")) {
            //    Datos modificados
            String datosRepresentante = request.getParameter("datosMod");
            System.out.println("Datos actualizados: " + datosRepresentante);
            
            //Crea la sesion del representante 
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("datosRepresentante", datosRepresentante);
            response.sendRedirect("../Vistas/PerfilRepresentante.jsp");
            
        }
    } catch (Exception e) {
        System.out.println("Excepción Catch  RepresentanteOpc");
    }
%>
