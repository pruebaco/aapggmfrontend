<%-- 
    Document   : controladorIngresarParalelo
    Created on : 13/11/2019, 17:17:22
    Author     : Angel
--%>

<%@page import="ggm.comun.estudiante"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.representante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    try {

        String datos = request.getParameter("datos"); //Datos obtenidos del LoginC
        String datosRepresentante = request.getParameter("datosR"); //Datos obtenidos de RepresentanteM del web service
        String opcion = request.getParameter("opcion"); //Opciones
        String asignarEst = request.getParameter("asignarEst");

        if (datos != null) {
            response.sendRedirect("../Modelos/RepresentanteM.jsp?datos=" + datos);
            System.out.println("Datos desde el login " + datos);

//        } else if (!datosDocente.equals("")) {
        } else if (datosRepresentante != null) {
            //Crea la sesion del representante 
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("datosRepresentante", datosRepresentante);

            response.sendRedirect("../Vistas/PanelRepresentanteV.jsp");
            System.out.println("Datos enviados a la vista " + datosRepresentante);

        } else if (opcion.equals("asignarEstudiantesRep")) {
            estudiante oEstudiante = new estudiante();
            Gson oGson = new Gson();
            oEstudiante.setStrCedula(asignarEst);
            String jsonEst = oGson.toJson(oEstudiante);
            response.sendRedirect("../Modelos/RepresentadosM.jsp?jsonEst=" + jsonEst + "&opcion=" + opcion);
        } else if (opcion.equals("guardarDatosM")) {
            String idEstudiante = request.getParameter("idEstudiante");
            String cedulaEst = request.getParameter("cedulaEst");
            String datosEstudiante = request.getParameter("datosEstudiante");
            
            response.sendRedirect("../Modelos/RepresentadosM.jsp?opcion=" + opcion + "&datosEstudiante=" + datosEstudiante + "&idEstudiante=" + idEstudiante + "&cedulaEst=" +cedulaEst);
        }

    } catch (Exception e) {

    }
%>