<%-- 
    Document   : DocenteOpC
    Created on : 19/11/2019, 11:49:23
    Author     : Angel
--%>

<%@page import="ggm.comun.paralelo"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String opcion = request.getParameter("opcion");
    System.out.println("Opicion para mostrar datos : " + opcion);
    try {
        //        Mostrar el perfil del docente
        if (opcion.equals("mostrarPerfil")) {
            response.sendRedirect("../Vistas/PerfilDocenteV.jsp");
        } else if (opcion.equals("listarParalelosAct")) {
            response.sendRedirect("../Modelos/DocenteM.jsp?opcion=" + opcion);
            System.out.println("Opcion controlador " + opcion);
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  DocenteOpC");
    }
%>
