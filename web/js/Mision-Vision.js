/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    misionVision();
});


function misionVision() {
    var contenido = "<div id=\"content-mision\" class=\"col-xs-8\" ALIGN=\"justify\">\n" +
            "                            <p>\n" +
            "                            <h2 align=\"center\">Misión</h2>\n" +
            "\n" +
            "\n" +
            "                            La Unidad Educativa “Dr. Gabriel García Moreno”, somos una institución católica\n" +
            "                            que ofrece un servicio educativo de calidad y calidez, que contribuye como factor \n" +
            "                            estratégico de justicia social que los forma como sujetos competentes, para acceder \n" +
            "                            a mejores condiciones de vida, aprendan a vivir en forma solidaria, democrática y ser\n" +
            "                            capaces de transformar su entorno.\n" +
            "                            </p>\n" +
            "\n" +
            "                        </div>\n" +
            "                        <div id=\"content-vision\" class=\"col-xs-8\" ALIGN=\"justify\">\n" +
            "                            <p>\n" +
            "                            <h2 align=\"center\"> Visión </h2>\n" +
            "\n" +
            "\n" +
            "                            Consolidar a la educación como eje fundamental de desarrollo para alcanzar niveles\n" +
            "                            de excelencia con sentido ético-cristiano con el compromiso y esfuerzo de la comunidad \n" +
            "                            educativa y sociedad brindando un servicio eficiente y eficaz que satisfaga plenamente \n" +
            "                            las necesidades y expectativas de los educandos. \n" +
            "                            </p>\n" +
            "\n" +
            "                        </div>";
    $('#contenidoDinamico').html(contenido);
}





