<%-- 
    Document   : Reportepdfs
    Created on : 07/11/2019, 12:00:12
    Author     : Angel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/Prueba.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!--librerias del jsPDF-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
        <script src="../js/jspdf.plugin.autotable.min.js"></script>
        <!--        <script src=”..//js/jspdf.debug.js”></script>
                <script src=”..//js/jspdf.min.js”></script>-->
        <title>Reporte</title>
    </head>
    <body>
        <button type="input" name="btn_pdf" id="btn_pdf" style='font-size:36px; color:blue; background-color: transparent; border:0; '><i class='far fa-file-pdf'></i></button>
        <a href="javascript:pruebaDivAPdf()" class="button">Pasar a PDF</a>
        <div id="encabezado">
            <header>
                <h1 style="text-align: center">Esto es un DIV impreso en PDF </h1>
            </header>  
            </div>
        <div id="pie-pagina">
            <footer>
                Pie de pagina
            </footer>
</div>
        
        <script>
            function pruebaDivAPdf() {
                var pdf = new jsPDF('p', 'pt', 'letter');
                source = $('#encabezado')[0];
//            source = $('#pie-pagina')[0];
                specialElementHandlers = {
                    '#bypassme': function (element, renderer) {
                        return true
                    }
                };
                margins = {
                    top: 80,
                    bottom: 60,
                    left: 40,
                    width: 522
                };

                pdf.fromHTML(
                        source,
                        margins.left, // x coord
                        margins.top, {// y coord
                            'width': margins.width,
                            'elementHandlers': specialElementHandlers
                        },
                        function (dispose) {
                            pdf.save('Prueba.pdf');
                        }, margins
                        );
            }
        </script>

        <script src="reportepdf.js"></script>
    </body>
</html>
