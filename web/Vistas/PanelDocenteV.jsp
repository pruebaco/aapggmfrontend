<%-- 
    Document   : PanelDocenteV
    Created on : 13/11/2019, 18:47:47
    Author     : Angel
--%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="ggm.comun.docente"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
//    String ListaESt = request.getParameter("ListaESt"); //Data de la lista de estudiantes
//    System.out.println("Verificar estudiante: " + ListaESt);
    String json = request.getParameter("json");
//    try {
//        if (ListaESt.equals("1")) {
//            System.out.println("Exito");
//            response.setStatus(200);
//            response.setContentType("text/plain");
//            response.getWriter().write(ListaESt);
//
//        }
//    } catch (Exception e) {
//
//    }
%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/vistadocente.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!--librerias del jsPDF-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
        <script src="../js/jspdf.plugin.autotable.min.js"></script>

        <title>Docente</title>
        <!--Estilos de la tablas para listar los estudiantes-->       
        <style>
            .table .thead-dark th{
                background-color: #004050;
                text-align: center;
            }
            /*            #container,#buttondiv{
                            margin:0 auto;
                            width:80%;
                            overflow:auto;
                        }*/
            .badrowcount {
                background-color: coral;
            }
            .notnumeric {
                background-color: yellow;
            }
        </style>

    </head>
    <body>
        <header >
            <div class="page-header" style="background:#252932; padding-left: 20px; padding-right: 20px" >
                <div class="form-row">

                    <div class="col-md-2">
                        <img class="center-block" src="../images/logo.jpg" width="55px" height="70px"
                             style="margin-left: 40%; margin-top: 5%; margin-bottom: 3% ">
                    </div>
                    <div class="col-md-8">
                        <h1 style="width:100%; margin-left: 1%; margin-top:15px; color:#f2f2f2; "> Unidad Educativa "Dr. Gabriel García Moreno" </h1>
                    </div>
                    <div class="col-md-2">
                        <img class="center-block" src="../images/logo2.jpg" width="55px" height="70px"
                             style=" margin-top: 5%; margin-bottom: 3%; margin-left: 25%;">
                    </div>
                </div>
            </div>
        </header>
        <br>
        <!--incio -->
        <div class="container">
            <div class="row">
                <div id="accordian" class="col-md-3">
                    <h1 style="text-align: center">  Menú </h1>
                    <br>
                    <ul>
                        <li>
                            <h3><span class='fas fa-id-card' style='font-size:36px' ></span>  Información académica</h3>
                            <ul>
                                <li><a href="#modalnuevoPerioAca" data-toggle="modal" data-target=".modal-nuevoPerioAca"> Periodo académico </a></li>
                                <li><a onclick="listaParalelos(); return false" href="#modalEstudiante" data-toggle="modal" data-target=".modal-estudiante"> Paralelos</a></li>
                                <li><a onclick="listarMaterias(); return false" href="#modalMaterias" data-toggle="modal" data-target=".modal-materia"> Mis Materias</a></li>
                                <!--<li><a onclick="listarActividades(); return false" href="#modalListarActividades" data-toggle="modal" data-target=".modal-listarActividades"> Activideades académicas </a></li>-->
                            </ul>
                        </li>
                        <!-- we will keep this LI open by default -->
                        <li class="active">
                            <h3><span  class='far fa-calendar-alt' style='font-size:36px'></span>  Actividades & Tareas</h3>
                            <ul>
                                <li><a onclick="listarActividades(); return false" href="#modalListarActividades" data-toggle="modal" data-target=".modal-listarActividades"> Activideades académicas </a></li>
                                <!--<li><a onclick="listarParalelosAct(); return false" href="#modalEstadoActividades" data-toggle="modal" data-target=".modal-estadoActividades"> Estado de actividades </a></li>-->
                                <li><a href="../Controladores/DocenteOpC.jsp?opcion=listarParalelosAct">Estado de actividades</a></li>
                                <br>
                            </ul>
                        </li>
                        <li>
                            <h3><span class='fas fa-book-open' style='font-size:36px'></span> Estudiantes</h3>
                            <ul>
                                <li><a href="../Controladores/EstudiantesC.jsp?opcion=listarParalelosDoc">Notas</a></li>
                                <br>
                            </ul>
                        </li>
                        <li>
                            <h3><span class="fa fa-file-pdf-o" style="font-size:36px"></span>  Reportes</h3>
                            <ul>
                                <li><a href="../Vistas/ReporteListaEstV.jsp">Lista de estudiantes</a></li>
                                <!--                                <li><a href="#">Aprobados</a></li>
                                                                <li><a href="#">Reprobados</a></li>-->
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--jQuery para desplegar menu--> 
                <script>
                    /*jQuery time*/
                    $(document).ready(function () {
                        $("#accordian h3").click(function () {
                            //slide up all the link lists
                            $("#accordian ul ul").slideUp();
                            //slide down the link list below the h3 clicked - only if its closed
                            if (!$(this).next().is(":visible"))
                            {
                                $(this).next().slideDown();
                            }
                        })
                    })
                </script>
                <div id="contenido" class="col-md-8" ALIGN="justify">
                    <div id="barra-superior" class="form-group row"> 
                        <h4 style=" color: white;"> &nbsp;&nbsp; Docente </h4>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Cuenta</button>
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">

                                <a class="dropdown-item" href="../Controladores/DocenteOpC.jsp?opcion=mostrarPerfil">Perfil personal</a>
                                <a class="dropdown-item" href="../index.jsp">Salir</a>
                            </div>
                        </div>

                    </div>
                    <div  id="contenidoDinamico">
                        <!--Mision vision-->
                    </div>
                </div>

            </div>
        </div>
        <!--Modal nuevo periodo academico INICIO--> 

        <div class="modal fade modal-nuevoPerioAca" id="modalnuevoPerioAca" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050" >
                        <h5 class="modal-title" id="periodoAca" style="text-align: center; color:#ffffff" >Periodo académico.</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <br>
                        <!--<form  action="../Controladores/DocenteC.jsp" method="get" enctype="text/plain">-->
                        <form   method="get" enctype="text/plain">

                            <div class="form-row">

                                <div class="col-md-8 mb-4">
                                    <label for="inputState" style="font-weight: bold;"> &nbsp;Nombre del periodo académicoa</label>
                                    <input type="text" class="form-control" id="PeriodoAca" name="PeriodoAca" placeholder="Septiembre 2019 / Julio 2020"  required>
                                    <div class="invalid-feedback">Ingrese el nombre del Periodo académico </div>
                                </div>

                            </div>
                            <br>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <input type="button" name="btn_guardarP"  class="btn btn-primary" value="Guardar" id="btn_guardarPeriodo" onclick="ingresarPeriodoAca();" >
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--Modal periodo academico FIN-->
        <!--Modal para listar paralelos e importar lista de estudiantes-->

        <div class="modal fade modal-estudiante" id="modalEstudiante" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050">
                        <h5 class="modal-title" id="paraleloModalLabel" style="text-align: center; color:#ffffff" >ESTUDIANTES</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3> Lista de estudiantes </h3>
                        <br>

                        <div class="form-row">
                            <label for="inputState" style=" padding: 6px; " > Paralelos </label>
                            <div class="form-group col-md-3">
                                <select onChange="listaEstudiantes(); return false" name="paralelos" id="opcionesParalelos" class="custom-select" value="" > 
                                    <option value="">Seleccionar paralelo</option>
                                </select>
                            </div>
                            <div class=" col-md-6">

                            </div>
                            <div class="form-group col-md-3">
                                <button onclick="cerrarModal(); return false" type="button" class="btn btn-outline-info"  value="" data-toggle="modal" data-target=".modal-paralelo"  style="color: #161615;" >Nuevo paralelo </button>
                            </div>
                        </div>

                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >
                                <div class="form-row">
                                    <div class="col-11">

                                    </div>
                                    <div class="ed-item web-100 main-center cross-center col-md-1">
                                        <button type="input" name="btn_pdf" id="btn_pdf" style='font-size:30px; color:blue; background-color: transparent; border:0; '><i class='far fa-file-pdf'></i></button>
                                    </div>  
                                </div>
                                <div style="margin:auto; " id="contenidoDinamico" class='col-xs-12'>
                                    <div class='form-group row'>
                                        <div class='col-xs-12'>
                                            <table class="table table-bordered" id="tablaEstudiantes">
                                                <thead style="background: #abdde5 ">
                                                    <tr>
                                                        <th style="width: 3%;"></th>
                                                        <td ><b>CÉDULA</b></td>
                                                        <td ><b>NOMBRES</b></td>
                                                        <td ><b>APELLIDO</b></td>
                                                        <th style="width: 3%;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaListaEstudiantes" style="color: #000706; font-size:85%;">
                                                    <tr class="hide">
                                                        <td style="width: 2%;"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fin-->

        <!--Pantalla modal para ingresar nuevo paralelo-->
        <!--Inicio-->
        <div class="modal fade modal-paralelo" id="modalparalelo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050">
                        <h5 class="modal-title" id="paralelo" style="text-align: center; color:#ffffff" >Paralelo</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3> Ingresar nuevo paralelo </h3>
                        <br>
                        <!--<form  action="../Controladores/DocenteC.jsp" method="get" enctype="text/plain">-->
                        <form   method="get" enctype="text/plain">

                            <div class="form-row">
                                <div class="col-md-4 mb-4">
                                    <label for="inputState"> &nbsp;Paralelo</label>
                                    <input type="text" class="form-control" id="paraleloNuevo" name="paraleloNuevo" placeholder="Nuevo paralelo"  required>
                                    <div class="invalid-feedback">Ingrese el paralelo</div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <label for="inputState"> Descripción</label>
                                    <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción del paralelo" required>
                                    <div class="invalid-feedback">Descripciòn del paralelo</div>
                                </div>
                            </div>
                            <br>
                            <div >
                                <input type="file" id="inputfile" />
                                <input type="button" id="viewfile" class="bn alert-heading" value="Importar archivo" />
                                <br><br>
                            </div>
                            <div class="container" id="container">
                                <table class="table table-bordered" id="tableMain" >
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Cédula</th>
                                            <th scope="col">Nombres</th>
                                            <th scope="col">Apellidos</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <input  type="button" name="btn_guardar01"  class="btn btn-primary" value="Guardar" id="btn_guardar" >
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--Fin--> 

        <!--Modal de Materias INICIO-->
        <!--listar las materias-->
        <div class="modal fade modal-materia" id="modalMaterias" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050">
                        <h5 class="modal-title" id="materiaModalLabel" style="color:#ffffff " >MIS MATERIAS </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#ffffff;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3> Lista de materias </h3>
                        <br>
                        <div>
                            <div class="form-row">
                                <div class="form-group col-md-11">
                                    <button type="button" class="btn btn-outline-info" value="" data-toggle="modal" data-target=".modal-nuevaMateria" onclick="listarParalMat();
                                            return false;" style="color: #161615;" > Nueva materia</button>

                                </div>
                                <!--                                <div class="ed-item web-100 main-center cross-center col-md-1">
                                                                    <button type="input" name="btn_pdf" id="btn_pdf" style='font-size:30px; color:blue; background-color: transparent; border:0; '><i class='far fa-file-pdf'></i></button>
                                                                </div>-->
                            </div>

                        </div>
                        <br>
                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >
                                <div style="margin:auto; " id="contenidoDinamico" class='col-xs-12'>
                                    <div class='form-group row'>
                                        <div class='col-xs-12'>
                                            <br>            
                                            <table class="table table-bordered" id="tablaMaterias">
                                                <thead style="background: #abdde5 ">
                                                    <tr>
                                                        <th style="width: 3%;"></th>
                                                        <td ><b>NOMBRES</b></td>
                                                        <td ><b>DESCRIPCIÓN</b></td>
                                                        <th style="width: 3%;"></th>
                                                    </tr>
                                                </thead>
                                                <br>
                                                <tbody id="tablaMateriasDoc" style="color: #000000; font-size:85%;">
                                                    <tr class="hide">
                                                        <td style="width: 2%;"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!--Modal nueva materia--> 

        <div class="modal fade modal-nuevaMateria" id="modalNuevamateria" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050" >
                        <h5 class="modal-title" id="materia" style="text-align: center; color:#ffffff " >Materia</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3> Ingresar nueva materia </h3>
                        <br>
                        <!--<form  action="../Controladores/DocenteC.jsp" method="get" enctype="text/plain">-->
                        <form   method="get" enctype="text/plain">
                            <div class="form-row">
                                <div class="col-md-4 mb-4">
                                    <label for="inputState" style="font-weight: bold;"> &nbsp;Nombre </label>
                                    <input type="text" class="form-control" id="txtMateria" name="txtMateria" placeholder="Nueva materia"  required>
                                    <div class="invalid-feedback">Ingrese el nombre de la materia</div>
                                </div>
                                <div class="col-md-5 mb-4">
                                    <label for="inputState" style="font-weight: bold;"> Descripción</label>
                                    <input type="text" class="form-control" id="txtDescripcionMat" name="txtDescripcionMat" placeholder="Descripción de la materia" required>
                                    <div class="invalid-feedback">Descripciòn de la materia</div>
                                </div>
                                <div class="col-md-3 mb-4" style="font-weight: bold;">
                                    <label for="inputState"> &nbsp;Paralelos </label>
                                    <select name="paralelos" id="opcionesParalelosMat1" class="custom-select" required > 
                                        <option value="">Seleccionar paralelo</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                <input type="button" id="btn_guardarPara" name="btn_guardarPara"  class="btn btn-primary" value="Guardar" onclick="ingresarMateria();">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--INICIO Modal para listar actividades adacemicas-->
        <div class="modal fade modal-listarActividades" id="modalListarActividades" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050" >
                        <h5 class="modal-title" id="actividad" style="text-align: center; color:#ffffff " >Actividades académicas </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3 style="text-align: center;">Lista de actividades académicas</h3>
                        <br>
                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >
                                <div style="margin:auto; " id="contenidoDinamico" class='col-xs-12'>
                                    <div class='form-group row'>
                                        <div class='col-xs-12'>
                                            <br>            
                                            <table class="table table-bordered" id="tablaActividades">
                                                <thead style="background: #abdde5 ">
                                                    <tr>
                                                        <th style="width: 3%;"></th>
                                                        <td ><b>NOMBRES</b></td>
                                                        <td ><b>FECHA REALIZADO</b></td>
                                                        <td ><b>FECHA PLAZO</b></td>
                                                        <td ><b>TIPO</b></td>
                                                        <th style="width: 3%;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaActividadesDoc">
                                                    <tr class="hide">
                                                        <td style="width: 2%;"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--Modal Estado de actividades--> 
        <div class="modal fade modal-estadoActividades" id="modalEstadoActividades" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050" >
                        <h5 class="modal-title" id="actividad" style="text-align: center; color:#ffffff " >Estado de actividades </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 style="text-align: center;">Lista de actividades académicas por estado</h5>
                        <br>
                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >
                                <div class="form-row" style="margin-top: 3%; margin-bottom: 3%;">
                                    <div class="col-sm-3" style="margin-left: 2%;"> 
                                        <b> <label for="inputState"> &nbsp;Paralelos </label> </b>
                                        <select  name="paralelos" id="opcionesParalelosDoc" class="custom-select"  required=""> 
                                            <option  value="" >Seleccionar</option>

                                        </select>
                                    </div>
                                    <div class="col-sm-3 " style=" margin-left: 2%;">
                                        <b> <label for="inputState"> &nbsp;Materia</label> </b>
                                        <select name="materias" id="opcionesMateriasDoc" class="custom-select" required > 
                                            <option value="">Seleccionar</option>

                                        </select>
                                    </div>
                                    <div class="col-sm-5" style="margin-left: 2%;">
                                        <b><label for="inputState"> &nbsp;Activides</label> </b>
                                        <select name="materias" id="opcionesMateriasDoc" class="custom-select" required > 
                                            <option value="">Seleccionar</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                        <br>
                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >
                                <div style="margin:auto; " id="contenidoDinamico" class='col-xs-12'>
                                    <div class='form-group row'>
                                        <div class='col-xs-12'>
                                            <br>            
                                            <table class="table table-bordered" id="tablaActividades">
                                                <thead style="background: #abdde5 ">
                                                    <tr>
                                                        <th style="width: 3%;"></th>
                                                        <td ><b>NOMBRES</b></td>
                                                        <td ><b>FECHA REALIZADO</b></td>
                                                        <td ><b>FECHA PLAZO</b></td>
                                                        <td ><b>TIPO</b></td>
                                                        <th style="width: 3%;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaActividadesDoc">
                                                    <tr class="hide">
                                                        <td style="width: 2%;"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--Script--> 
        <script type="text/javascript">
            //mesaje 
            $(document).on('click', 'input[type="button"]', function (event) {
                let id = this.id;
                if (id = '#btn_guardar') {
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Operación realizada'
                    })
                }
            });

//            Ingresar nuevo periodo académico
            function ingresarPeriodoAca() {
                var nombreAca = document.getElementById('PeriodoAca').value;
//                alert( nombreAca);
                $.ajax({
                    url: "../Controladores/DocenteC.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: "ingresarPeriodoAca",
                        periodoAcade: nombreAca
                    },
                    success: function (response) {
                        $('#modalnuevoPerioAca').modal('hide');
                        if (response.equals('1'))
                        {

                            document.getElementById('PeriodoAca').value = '';
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: 'Periodo Académico ingresado con éxito'
                            })

                        } else
                        {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'Se produjo un error'
                            })
                            $('#modalnuevoPerioAca').modal('hide');
                        }

                    },
                    error: function (error) {
                        console.log(error);
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Se produjo un error'
                        })
                    }
                });
            }
//    Listar Paralelos para ingresar las materias 
            function listarParalMat() {
                $.ajax({
                    url: "../Modelos/MateriasM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listadoParalelosMat'
                    },
                    success: function (resultado) {
                        var paralelos = JSON.parse(resultado);
                        var html = "";
                        paralelos.forEach(function (paralelo) {

                            html += "<option value='" + paralelo.intIdParalelo + "," + paralelo.intIdPeriodo + "'>" + paralelo.strParalelo + "</option>";
                        });
                        $('#opcionesParalelosMat1').html(html);
                        $('#modalNuevamateria').modal('show');
                        $('#modalMaterias').modal('hide');
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
            // Ingresar materia
            function ingresarMateria() {
                var nombreMat = document.getElementById('txtMateria').value;
                var descripMat = document.getElementById('txtDescripcionMat').value;
                var ParalMat = document.getElementById('opcionesParalelosMat1').value;
                $.ajax({
                    url: "../Controladores/DocenteC.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {opcion: "ingresarMateria", nombreMateria: nombreMat, descripcionMat: descripMat, paraleloMat: ParalMat},
                    success: function (response) {

                        if (response > 0)
                        {
                            $('#modalNuevamateria').modal('hide');
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: 'Materia ingresada con Exito'
                            })
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: "Se produjo un error"
                            })
                        }

                    },
                    error: function (error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: "Se produjo un error"
                        })
                    }
                });
            }

            //Funcion para listar lista de paralelos
            function listaParalelos() {
                $.ajax({
                    url: "../Modelos/ParaleloM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        accion: 'listadoParalelos'
                    },
                    success: function (resultado) {
                        var paralelos = JSON.parse(resultado);
                        var html = "";
                        paralelos.forEach(function (paralelo) {
                            html += "<option value='" + paralelo.intIdParalelo + "'>" + paralelo.strParalelo + "</option>";
                        });
                        $('#opcionesParalelos').html(html);
                        $('#modalEstudiante').modal('show');
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            //            Listar lista de estudiantes 
            function listaEstudiantes() {
                var opParalelo = document.getElementById('opcionesParalelos').value;
                $.ajax({
                    url: "../Modelos/ParaleloM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        accion: 'listaEstudiantes',
                        opParal: opParalelo
                    },
                    success: function (resultado) {
//                        alert(resultado);
                        var estudiantes = JSON.parse(resultado);
                        var html = "";
                        estudiantes.forEach(function (estudiante) {
                            html += "<tr>";
                            html += "<td></td>";
                            html += "<td>" + estudiante.strCedula + "</td>";
                            html += "<td>" + estudiante.strNombres + "</td>";
                            html += "<td>" + estudiante.strApellidos + "</td>";
                            html += "<td></td>";
                            html += "</tr>";

                        });
                        $('#tablaListaEstudiantes').html(html);
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }
//Cerrar la modal
            function cerrarModal() {
                $("#modalEstudiante").modal('hide');//ocultamos el modal
            }
//Listar activiades academicas 
            function listarMaterias() {

                $.ajax({
                    url: "../Modelos/MateriasM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarMateriasDoc'
                    },
                    success: function (resultado) {

                        if (resultado < 0) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: "No existen materias para ser visualizadas"
                            })

                        } else {
                            var materias = JSON.parse(resultado);
                            var html = "";
                            materias.forEach(function (materia) {
                                html += "<tr>";
                                html += "<td></td>";
                                html += "<td>" + materia.strNombreMAteria + "</td>";
                                html += "<td>" + materia.strDescripcionMateria + "</td>";
                                html += "<td></td>";
                                html += "</tr>";
                            });
                            $('#tablaMateriasDoc').html(html);
                        }
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }

//Listar activiades academicas 
            function listarActividades() {

                $.ajax({
                    url: "../Modelos/ActividadesM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarActividadesDoc'
                    },
                    success: function (resultado) {

                        if (resultado < 0) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: "No existen actividas para ser visualizadas"
                            })

                        } else {
                            var activiades = JSON.parse(resultado);
//                        alert(activiades)
                            var html = "";
                            activiades.forEach(function (actividad) {
                                html += "<tr>";
                                html += "<td></td>";
                                html += "<td>" + actividad.strNombre + "</td>";
                                html += "<td>" + actividad.strFechaRealizado + "</td>";
                                html += "<td>" + actividad.strFechaPlazo + "</td>";
                                html += "<td>" + actividad.strTipo + "</td>";
                                html += "<td></td>";
                                html += "</tr>";
                            });
                            $('#tablaActividadesDoc').html(html);
                        }

                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }

//            Listar paralelos para ver el estado de las actividades
//            function listarParalelosAct() {
//
//                $.ajax({
//                    url: "../Modelos/DocenteOpC.jsp",
//                    type: 'GET',
//                    dataType: 'text',
//                    data: {
//                        opcion: 'listarParalelosAct'
//                    },
//                    success: function (resultado) {
//
//                    },
//                    error: function (error) {
//                        console.log(error);
//                    }
//                });
//            }
        </script>

        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/sweetalert2.all.min.js"></script>
        <script src="../js/Mision-Vision.js"></script>
        <script src="../js/importarEstudiantes.js"></script>
        <!--<script src="../Reportes/ListaMaterias.js"></script>-->
        <script src="../Reportes/ListaEstudiantesParalelo.js"></script>
    </body>
</html>
