<%-- 
    Document   : PanelRepresentanteV
    Created on : 19/12/2019, 14:25:31
    Author     : Angel
--%>

<%@page import="ggm.comun.representante"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/vistarepresentante.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <title>Representante</title>
    </head>
    <body>
        <header >
            <div class="page-header" style="background:#252932; padding-left: 20px; padding-right: 20px" >
                <div class="form-row">

                    <div class="col-md-2">
                        <img class="center-block" src="../images/logo.jpg" width="55px" height="70px"
                             style="margin-left: 40%; margin-top: 5%; margin-bottom: 3% ">
                    </div>
                    <div class="col-md-8">
                        <h1 style="width:100%; margin-left: 1%; margin-top:15px; color:#f2f2f2; "> Unidad Educativa "Dr. Gabriel García Moreno" </h1>
                    </div>
                    <div class="col-md-2">
                        <img class="center-block" src="../images/logo2.jpg" width="55px" height="70px"
                             style=" margin-top: 5%; margin-bottom: 3%; margin-left: 25%;">
                    </div>
                </div>
            </div>
        </header>
        <br>
        <br>
        <!--incion-->
        <div class="container">
            <div class="row">
                <div id="accordian" class="col-md-3">
                    <h1 style="text-align: center">  Menú </h1>
                    <br>
                    <ul>
                        <li>
                            <h3><span class='fas fa-user-friends' style='font-size:36px' ></span>  Representados</h3>
                            <ul>
                                <li><a onclick="listarParalelos(); return false" href="#modalRepresentados" data-toggle="modal" data-target=".modal-representados"> Asignar representado</a></li>
                                <li><a onclick="listarRepresentados(); return false" href="#modalMisRepresentados" data-toggle="modal" data-target=".modal-Misrepresentados"> Mis representados</a></li>
                            </ul>
                        </li>
                        <li>
                            <h3><span  class='far fa-calendar-alt' style='font-size:36px'></span>  Actividades & Tareas</h3>
                            <ul>
                                <li><a onclick="listarEstudiantes(); return false" href="#modalNotasCualitativas" data-toggle="modal" data-target=".modal-NotasCualitativas"> Notas cualitativas</a></li>
                            </ul>
                        </li>
                    </ul>

                </div>
                <div id="contenido" class="col-md-8" ALIGN="justify">
                    <div id="barra-superior" class="form-group row"> 
                        <h4 style=" color: white;"> &nbsp;&nbsp; Representante </h4>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary">Cuenta</button>
                            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="../Controladores/RepresentanteOpC.jsp?opcion=mostrarPerfil">Perfil personal</a>
                                <a class="dropdown-item" href="../index.jsp">Salir</a>
                            </div>
                        </div>
                    </div>
                    <div  id="contenidoDinamico">
                        <!--Mision vision-->
                    </div>
                </div>

            </div>
        </div>
        <!--Modades-->
        <div class="modal fade modal-representados" id="modalRepresentados" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050">
                        <h5 class="modal-title" id="paraleloModalLabel" style="text-align: center; color:#ffffff" >REPRESENTADOS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3> Lista de estudiantes </h3>
                        <br>
                        <form  method="get" enctype="text/plain" >
                            <div class="form-row">
                                <label for="inputState" style=" padding: 6px; " > Paralelos </label>
                                <div class="form-group col-md-3">
                                    <select onChange="listarEstudiantes(); return false" name="paralelos" id="opcionesParalelos" class="custom-select" value="" > 
                                        <option value="">Seleccionar</option>
                                    </select>
                                </div>

                                <div class=" col-md-4" style="padding-left: 36%;"></div>
                                <div class=" col-md-3">
                                    <input type="text" class="form-control" id="txtAsignar" name="txtAsignar" placeholder="Cédula" value="" required>
                                </div>
                                <div class="form-group col-md-1">
                                    <input type="button" id="btnAsignar" name="btn_asignar"  class="btn btn-outline-info" value="Asignar"  style="color: #161615;" onclick="asignarEstudiantes(); return false">
                                </div>

                            </div>
                        </form>

                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >

                                <div style="margin:auto; " id="contenidoDinamico" class='col-xs-12'>
                                    <div class='form-group row'>
                                        <div class='col-xs-12'>
                                            <br>
                                            <table class="table table-bordered" id="tablaEstudiantes">
                                                <thead style="background: #abdde5 ">
                                                    <tr>
                                                        <th style="width: 3%;"></th>
                                                        <td ><b>CÉDULA</b></td>
                                                        <td ><b>NOMBRES</b></td>
                                                        <td ><b>APELLIDO</b></td>
                                                        <th style="width: 3%;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaListaEstudiantes" style="color: #000706; font-size:85%;">
                                                    <tr class="hide">
                                                        <td style="width: 2%;"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal mis representados-->

        <div class="modal fade modal-Misrepresentados " id="modalMisRepresentados" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050">
                        <h5 class="modal-title" id="paraleloModalLabel" style="text-align: center; color:#ffffff" >MIS REPRESENTADOS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body ">
                        <br>
                        <h5> Representados (Hij@s) </h5>
                        <br>
                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >
                                <div style="margin:auto; " id="contenidoDinamico" class='col-xs-12'>
                                    <div class='form-group row'>
                                        <div class='col-xs-12'>
                                            <br>
                                            <table class="table table-bordered" id="tablaRepresentados">
                                                <thead style="background: #abdde5 ">
                                                    <tr>
                                                        <th style="width: 3%;"></th>
                                                        <td ><b>CÉDULA</b></td>
                                                        <td ><b>NOMBRES</b></td>
                                                        <td ><b>APELLIDO</b></td>
                                                        <td ><b>EDAD</b></td>
                                                        <td ><b>TEL. DE REFERENCIA</b></td>
                                                        <td colspan="2"><b>ACCI&Oacute;N</b></td>
                                                        <th style="width: 3%;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaListaRepresentados" style="color: #000706; font-size:85%;">
                                                    <tr class="hide">
                                                        <td style="width: 2%;"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true" ></td>
                                                        <td  contenteditable="true" ></td>
                                                        <td> </td>
                                                        <td> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Listar notas cualitativas de cada uno de los estudiantes-->
        <div class="modal fade modal-NotasCualitativas" id="modalNotasCualitativas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style=" height: 80%; " >
            <div class=" modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header" style="background: #004050">
                        <h5 class="modal-title" id="paraleloModalLabel" style="text-align: center; color:#ffffff" >NOTAS CUALITATIVAS DEL ESTUDIANTE</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <br>
                        <h5> Listado de actividades y notas cualitativas </h5>
                        <br>
                        <div class="form-row">
                            <b><label for="inputState" style=" padding: 6px; " > Estudiantes: </label></b>
                            <div class="form-group col-md-6">
                                <select onChange="listarNotasCualitativas(); return false" name="estudiantes" id="opcionesEstudiantes" class="custom-select" value="" > 
                                    <option value="Seleccionar">Seleccionar</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-auto main-section">
                            <div class="modal-content" >
                                <div style="margin:auto; " id="contenidoDinamico" class='col-xs-12'>
                                    <div class='form-group row'>
                                        <div class='col-xs-12'>
                                            <br>
                                            <table class="table table-bordered" id="tablaNotasCualitativas">
                                                <thead style="background: #abdde5 ">
                                                    <tr>
                                                        <th style="width: 3%;"></th>
                                                        <td ><b>NOMBRES</b></td>
                                                        <td ><b>FECHA REALIZADO</b></td>
                                                        <td ><b>FECHA PLAZO</b></td>
                                                        <td ><b>TIPO</b></td>
                                                        <td ><b>ESTADO</b></td>
                                                        <th style="width: 3%;"></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tablaListaNotasCualit" style="color: #000706; font-size:85%;">
                                                    <tr class="hide">
                                                        <td style="width: 2%;"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true"></td>
                                                        <td  contenteditable="true" ></td>
                                                        <td  contenteditable="true" ></td>
                                                        <td> </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--jQuery para desplegar menu--> 
        <script>
            /*jQuery time*/
            $(document).ready(function () {
                $("#accordian h3").click(function () {
                    //slide up all the link lists
                    $("#accordian ul ul").slideUp();
                    //slide down the link list below the h3 clicked - only if its closed
                    if (!$(this).next().is(":visible"))
                    {
                        $(this).next().slideDown();
                    }
                })
            })

//            Listar Paralelos
            function listarParalelos() {
                $.ajax({
                    url: "../Modelos/RepresentadosM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarParalelosRep'
                    },
                    success: function (resultado) {
                        var paralelos = JSON.parse(resultado);
                        var html = "";
                        paralelos.forEach(function (paralelo) {
                            html += "<option value='" + paralelo.intIdParalelo + "'>" + paralelo.strParalelo + "</option>";
                        });
                        $('#opcionesParalelos').html(html);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            //            Listar lista de estudiantes 
            function listarEstudiantes() {
                var opParalelo = document.getElementById('opcionesParalelos').value;
                $.ajax({
                    url: "../Modelos/RepresentadosM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarEstudiantesRep',
                        opParal: opParalelo
                    },
                    success: function (resultado) {
                        var estudiantes = JSON.parse(resultado);
                        var html = "";
                        estudiantes.forEach(function (estudiante) {
                            html += "<tr>";
                            html += "<td></td>";
                            html += "<td>" + estudiante.strCedula + "</td>";
                            html += "<td>" + estudiante.strNombres + "</td>";
                            html += "<td>" + estudiante.strApellidos + "</td>";
                            html += "<td></td>";
                            html += "</tr>";

                        });
                        $('#tablaListaEstudiantes').html(html);
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }
            //   Asignar estudiantes a los representantes
            function asignarEstudiantes() {
                var asignarEst = document.getElementById('txtAsignar').value;
                $.ajax({
                    url: "../Controladores/RepresentanteC.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'asignarEstudiantesRep',
                        asignarEst: asignarEst
                    },
                    success: function (response) {
                        if (response > 0) {

                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: 'Estudiante asignado'
                            })
                            document.getElementById('txtAsignar').value = '';
                        } else {
                            document.getElementById('txtAsignar').value = '';
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'Se produjo un error a la hora de asignar'
                            })
                        }
                    },
                    error: function (error) {
                        onsole.log(error);

                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Se produjo un error a la hora de asignar'
                        })
                    }
                });
            }
            //            Listar estudiantes por representante
            function listarRepresentados() {
                $.ajax({
                    url: "../Modelos/RepresentadosM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarRepresentados',
                    },
                    success: function (resultado) {
                        var estudiantes = JSON.parse(resultado);
                        var html = "";
                        estudiantes.forEach(function (estudiante) {
                            html += "<tr>";
                            html += " <input type='hidden' value='" + estudiante.strCedula + "'>";
                            html += "<td></td>";
                            html += "<td>" + estudiante.strCedula + "</td>";
                            html += "<td>" + estudiante.strNombres + "</td>";
                            html += "<td>" + estudiante.strApellidos + "</td>";
                            html += "<td style='width:14%;'>" + estudiante.intEdad + "</td>";
                            html += "<td style='width:14%;'>" + estudiante.strTelefonoReferencia + "</td>";
                            html += "<td><i onclick='modificar(this);' class='far fa-edit' style='font-size:24px; color:blue; cursor: auto'></i></td>";
                            html += "<td><i onclick='guardarDatosM(this);' class='far fa-save' style='font-size:24px; color:blue; cursor: auto '></i></td>";
                            html += " <input type='hidden' value='" + estudiante.intIdEstudiante + "'>";
                            html += "</tr>";

                        });
                        $('#tablaListaRepresentados').html(html);
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }
            //Activar los campos para ser modificados
            function modificar(event) {
                for (var i = 4; i < 8; i += 1) {
                    if (i < 6)
                    {
                        event.parentNode.parentNode.childNodes[i].innerHTML = "<input type='text' class='form-control' value='" + event.parentNode.parentNode.childNodes[i].textContent + "'>";
                    } else if (i === 6) {
                        event.parentNode.parentNode.childNodes[i].innerHTML = "<input type='number' class='form-control' min='6' max='19' step='1' value='" + event.parentNode.parentNode.childNodes[i].textContent + "'>";
                    } else if (i > 6) {
                        event.parentNode.parentNode.childNodes[i].innerHTML = "<input type='text' class='form-control' value='" + event.parentNode.parentNode.childNodes[i].textContent + "'>";
                    }
                }
            }
//            Guardar datos modificados 

            function guardarDatosM(event) {
                var array = [];
                for (var i = 4; i < 8; i += 1) {
                    if (i < 6) {
                        array.push(event.parentNode.parentNode.childNodes[i].childNodes[0].value);
                        event.parentNode.parentNode.childNodes[i].textContent = event.parentNode.parentNode.childNodes[i].childNodes[0].value;
                    } else if (i === 6) {
                        array.push(parseInt(event.parentNode.parentNode.childNodes[i].childNodes[0].value));
                        event.parentNode.parentNode.childNodes[i].textContent = event.parentNode.parentNode.childNodes[i].childNodes[0].value;
                    } else if (i > 6) {
                        array.push(event.parentNode.parentNode.childNodes[i].childNodes[0].value);
                        event.parentNode.parentNode.childNodes[i].textContent = event.parentNode.parentNode.childNodes[i].childNodes[0].value;
                    }

                }
                var cedulaEstudiante = event.parentNode.parentNode.firstElementChild.value;
                var id_estudiante = event.parentNode.parentNode.lastElementChild.value;
                var datosEst = JSON.stringify(array);

                console.log(datosEst);
                $.ajax({
                    url: "../Controladores/RepresentanteC.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {opcion: "guardarDatosM", datosEstudiante: datosEst, idEstudiante: id_estudiante, cedulaEst: cedulaEstudiante},
                    success: function (response) {
                        if (response > 0)
                        {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: 'Datos modificados'
                            })
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'Se produjo un error'
                            })
                        }
                    },
                    error: function (error) {
                        console.log('Error ' + error);
                    }
                });
            }

//            Listar actividades y notas cualitativas de cada estudiante
            function listarEstudiantes() {
                $.ajax({
                    url: "../Modelos/RepresentanteM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listaEstudiantes'
                    },
                    success: function (resultado) {
                        var estudiantes = JSON.parse(resultado);
                        var html = "";
                        estudiantes.forEach(function (estudiante) {
                            html += "<option value='" + estudiante.strCedula + "'>" + estudiante.strNombres + ' ' + estudiante.strApellidos + "</option>";
                        });
                        $('#opcionesEstudiantes').html(html);
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }
//Listar actividdes y notas cualitativas 
            function listarNotasCualitativas() {
                var opEstudiantes = document.getElementById('opcionesEstudiantes').value;
                console.log('Id del estudiante: ' + opEstudiantes);
                $.ajax({
                    url: "../Modelos/RepresentanteM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarNotasCualitativas',
                        opEstudiante: opEstudiantes
                    },
                    success: function (resultado) {
                        var actividades = JSON.parse(resultado);
                        var html = "";
                        actividades.forEach(function (actividad) {

                            html += "<tr>";
                            html += "<td></td>";
                            html += "<td>" + actividad.strNombre + "</td>";
                            html += "<td>" + actividad.strFechaRealizado + "</td>";
                            html += "<td>" + actividad.strFechaPlazo + "</td>";
                            html += "<td>" + actividad.strTipo + "</td>";
                            if (actividad.intIdClase === 1)
                            {
                                html += "<td>" + "No entregado" + "</td>";
                            } else if (actividad.intIdClase === 2) {
                                html += "<td>" + "Incompleto" + "</td>";
                            } else if (actividad.intIdClase === 3) {
                                html += "<td>" + "Entregado" + "</td>";
                            }
                            html += "<td></td>";
                            html += "</tr>";
                        });
                        $('#tablaListaNotasCualit').html(html);
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }
        </script>
        <!--<script src="../js/jquery-3.3.1.slim.min.js"></script>-->
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/sweetalert2.all.min.js"></script>
        <script src="../js/Mision-Vision.js"></script>
    </body>
</html>
