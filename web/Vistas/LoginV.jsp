<%-- 
    Document   : LoginV
    Created on : 13/11/2019, 18:24:32
    Author     : Angel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <!--FRAMEWORK BOOTSTRAP para el estilo de la pagina-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <!-- Los iconos tipo Solid de Fontawesome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
        <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/Login.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>Login</title>

    </head>
    <body>
        <div class="modal-dialog text-center">
            <div class="col-sm-8 main-section">
                <div class="modal-content">
                    <div class="col-12 user-img">
                        <img src="../img/avatarLogin.png" th:src="@{img/avatarLogin.png}"/>
                    </div>
                    <form role="form" class="needs-validation col-12" novalidate action="../Controladores/LoginC.jsp" method="GET">
                        <div class="form-group" id="user-group">
                            <input type="text" class="form-control" id="usrname" placeholder="Cédula" name="cedula" required/>
                            <div class="invalid-feedback">Ingrese el nombre de usuario</div>
                        </div>
                        <div class="form-group" id="contrasena-group">
                            <input type="password" class="form-control" id="psw" placeholder="Contraseña" name="contrasena" required/>
                            <div class="invalid-feedback">Ingrese la contraseña</div>
                        </div>
                        <button type="submit" name="btningresar" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i>  Ingresar </button>
                    </form>
                    <div class="col-12 forgot">
                        <a href="#">Recordar contrasena?</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
//             Validaciòn de los campos 
            (function () {
                'use strict';
                window.addEventListener('load', function () {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation col-12');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function (form) {
                        form.addEventListener('submit', function (event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
        <!--opciones-->
        <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
    </body>
</html>
