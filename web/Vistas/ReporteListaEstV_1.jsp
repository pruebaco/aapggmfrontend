<%-- 
    Document   : EstudiantesV
    Created on : 29/11/2019, 10:35:25
    Author     : Angel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!--librerias del jsPDF-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
        <script src="../js/jspdf.plugin.autotable.min.js"></script>
        <title>Reportes </title>
        <style>

            .table .thead-dark{

                color:#456789;
                font-size:10%;
            }
        </style>
    </head>
    <body>
        <header >
            <div class="page-header" >
                <div class="form-row">

                    <div class="col-md-2">
                        <a title="Logo 1" href="PanelDocenteV.jsp"><img class="center-block" src="../images/logo.jpg" width="55px" height="70px"
                                                                        style="margin-left: 165%; margin-top: 10%; margin-bottom: 3% "></a>
                    </div>
                    <div class="col-md-8">
                        <h5 style="width:100%; margin-left: 1%; margin-top:15px;  text-align: center; "> <b> UNIDAD EDUCATIVA </b> </h5>
                        <h5 style="width:100%; margin-left: 1%; margin-top:15px;  text-align: center;"> <b> “DR. GABRIEL GARCÍA MORENO” </b> </h5>
                        <h6 style="width:100%; margin-left: 1%; margin-top:15px;  text-align: center; "> La Matriz-Guano</h6>
                        <h4 style="width:100%; margin-left: 1%; margin-top:15px;  text-align: center; ">Lista de estudiantes </h4>
                    </div>
                    <div class="col-md-2">
                        <img class="center-block" src="../images/logo2.jpg" width="55px" height="70px"
                             style=" margin-top: 10%; margin-bottom: 3%; margin-left: -90%;">
                    </div>
                </div>
            </div>
        </header>
        <br>
        <br>
        <div class="col-md-9 main-section" style="margin: auto;">
            <div class="form-row" style="font-weight: bold; margin-left: 20%; ">
                <label for="inputState" style="padding: 7px; "> &nbsp;&nbsp;Paralelos </label>
                <div class="form-group col-md-3">
                    <select onChange="listaEstudiantes(); return false" name="paralelos" id="opcionesParalelos" class="custom-select" value="" > 
                        <option value="">Seleccionar paralelo</option>
                    </select>
                </div>
                <div class="ed-item web-100 main-center cross-center">

                    <button type="input" name="btn_pdf" id="btn_pdf" style='font-size:36px; color:blue; background-color: transparent; border:0; '><i class='far fa-file-pdf'></i></button>

                </div>
            </div>
            <br>

            <div class="col-lg-8 main-section">
                <div class="modal-content text-center" style="margin-left: 28%;">
                    <div style="margin: auto;" id="contenidoDinamico" class='col-xs-12'>
                        <div class='form-group row'>
                            <div class='col-xs-12'>
                                <br>            
                                <table class="table table-bordered" id="tablalistaEstudiante">
                                    <thead style="background: #abdde5 ">
                                        <tr>
                                            <th style="width: 3%;"></th>
                                            <td ><b>CÉDULA</b></td>
                                            <td ><b>NOMBRES</b></td>
                                            <td ><b>APELLIDO</b></td>
                                            <th style="width: 3%;"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tablaEstudiantes" style="color: #000706; font-size:85%;">
                                        <tr class="hide">
                                            <td style="width: 2%;"></td>
                                            <td  contenteditable="true"></td>
                                            <td  contenteditable="true"></td>
                                            <td  contenteditable="true"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <script type="text/javascript">

            $(document).ready(function listarParalelos() {

                $.ajax({
                    url: "../Modelos/ReportesM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listadoParalelos'
                    },
                    success: function (resultado) {
                        var paralelos = JSON.parse(resultado);
                        var html = "";
                        paralelos.forEach(function (paralelo) {
                            html += "<option value='" + paralelo.intIdParalelo + "'>" + paralelo.strParalelo + "</option>";
                        });
                        $('#opcionesParalelos').html(html);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });

//            Listar lista de estudiantes 
            function listaEstudiantes() {
                var opParalelo = document.getElementById('opcionesParalelos').value;
                $.ajax({
                    url: "../Modelos/ReportesM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listaEstudiantes',
                        opParal: opParalelo
                    },
                    success: function (resultado) {

                        var estudiantes = JSON.parse(resultado);
                        var html = "";
                        estudiantes.forEach(function (estudiante) {
                            html += "<tr>";
                            html += "<td></td>";
                            html += "<td>" + estudiante.strCedula + "</td>";
                            html += "<td>" + estudiante.strNombres + "</td>";
                            html += "<td>" + estudiante.strApellidos + "</td>";

                            html += "<td></td>";
                            html += "</tr>";

                        });
                        $('#tablaEstudiantes').html(html);
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            }


        </script>
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../Reportes/ListaEstudiantesR.js"></script>
    </body>
</html>
