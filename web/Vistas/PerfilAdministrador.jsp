<%-- 
    Document   : EstudiantesV
    Created on : 29/11/2019, 10:35:25
    Author     : Angel
--%>

<%@page import="ggm.comun.administrativo"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    administrativo oAdm = new administrativo();
    Gson oGson = new Gson();

    HttpSession sesion = request.getSession(true);
    String datosAdmi = sesion.getAttribute("datosAdministrador").toString();
    oAdm = oGson.fromJson(datosAdmi, administrativo.class);

%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/estilosMenuPerfil.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>Perfil personal </title>
    </head>
    <body>
 <nav class="navbar navbar-dark bg-dark navbar-expand-lg sticky-top">

            <nav class="navbar navbar-dark bg-dark">
                <a class="navbar-brand" href="#">
                    <img src="../img/Escudo.ico" width="30" height="30" class="d-inline-block align-top" alt="">
                    Administrador
                </a>
            </nav>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuAdmonistrador" aria-controls="menuAdmonistrador" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="menuAdmonistrador">
                <ul class="nav nav-item mr-auto" style="text-decoration: white; ">
                    <li class="nav-item">
                        <a class="nav-link active" href="PanelAdmonistradorV.jsp" style="color: #ffffff;">Inicio</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: #ffffff;">Datos personales</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="../Controladores/AdministrativoC.jsp?opcion=mostrarPerfil">Perfil</a>
                            <a class="dropdown-item" href="ModificarAdministradorV.jsp">Modificar</a>

                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: #ffffff;">Reportes</a>
                    </li>

                </ul>
                <div>
                    <a class="nav-link" href="../index.jsp" style="color: #ffffff;">Cerrar Sesión </a>
                </div>
            </div>
        </nav>
        <div class="contenido">
            <div class="col-md-12 main-section" style="margin: auto; margin-top: 4%;">
                <div class="col-md-9 main-section">
                    <div class="modal-content text-center col-md-4" style="margin-left: 12%; margin-bottom: -0.3%;">
                        <h3 style="text-align: left; color: blue; font-weight: bolder;" >Datos personales</h3>
                    </div>

                    <div class="modal-content text-center " style="margin-left: 12%;">
                        <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                            <label for="inputNombre" class="col-form-label" style="text-align: right; font-weight: bolder;">Nombres</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<%=oAdm.getStrNombre()%>" name="txtNombre" readonly>
                            </div>
                            <label for="inputApellido" class=" col-form-label" style="text-align: right; font-weight: bolder;">Apellidos</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrApellido()%>" name="txtApellido"readonly >
                            </div>
                        </div>
                        <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                            <label for="inputCedula" class=" col-form-label" style="text-align: right; font-weight: bolder;">Cédula</label>
                            <div class="col-sm-3" Style="padding-left: 3.5%;">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrCedula()%>" name="txtCedula" readonly>
                            </div>
                            <label for="inputEdad" class=" col-form-label" style="text-align: right; font-weight: bolder;">Edad </label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control"  value="<%=oAdm.getIntEdad()%>" name="txtEdad" readonly>
                            </div>
                            <label for="inputCargo" class="col-form-label" style="text-align: right; font-weight: bolder;">Cargo</label>
                            <div class="col-md-3">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrCargo() %>"  name="txtCargo" readonly>
                            </div>
                        </div>
                        <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                            <label for="inputCorreo" class="col-form-label" style="text-align: right; font-weight: bolder;">Correo</label>
                            <div class="col-sm-5" Style="padding-left: 3.5%;">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrCorreo()%>" name="txtCorreo" readonly>
                            </div>
                            <label for="inputTelefono" class=" col-form-label" style="text-align: right; font-weight: bolder;">Teléfono</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrTelefono()%>" name="txtTelefono" readonly>
                            </div>
                        </div>
                        <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                            <label for="inputCelular1" class="col-form-label" style="text-align: right; font-weight: bolder;">Celular 1</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrCelular1()%>" name="txtCelular1" readonly>
                            </div>
                            <label for="inputCelular2" class=" col-form-label" style="text-align: right; font-weight: bolder;">Celular 2</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrCelular2()%>"  name="txtCelular2" readonly>
                            </div>
                        </div>
                        <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >
                            <label for="inputTitulo1" class="col-form-label" style="text-align: right; font-weight: bolder;">Título 1</label>
                            <div class="col-sm-5" Style="padding-left: 3.5%;">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrTitulo1() %>"  name="txtTitulo1" readonly>
                            </div>
                            <label for="inputTitulo2" class="col-form-label" style="text-align: right; font-weight: bolder;">Título 2</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control"  value="<%=oAdm.getStrTitulo2() %>"  name="txtTitulo2" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>

    </body>
</html>
