<%-- 
    Document   : EstudiantesV
    Created on : 29/11/2019, 10:35:25
    Author     : Angel
--%>

<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.representante"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%
    representante oRep = new representante();
    Gson oGson = new Gson();

    HttpSession sesion = request.getSession(true);
    String datosRepresentante = sesion.getAttribute("datosRepresentante").toString();
    oRep = oGson.fromJson(datosRepresentante, representante.class);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/estilosMenuModificar.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>Modificar datos</title>
    </head>
    <body>
        <header >
            <div class="page-header" style="background:#252932; padding-left: 20px; padding-right: 20px" >
                <div class="form-row">

                    <div class="col-md-2">
                        <a title="Logo 1" href="PanelRepresentanteV.jsp"><img class="center-block" src="../images/logo.jpg" width="27.5px" height="35px"
                                                                              style="margin-left: 40%; margin-top: 5%; margin-bottom: 3% "></a>
                    </div>
                    <div class="col-md-8">
                        <h3 style="  text-align:center; margin-left: auto; margin-right: auto; margin-top:15px; color:#f2f2f2; "> Unidad Educativa "Dr. Gabriel García Moreno" </h3>
                    </div>
                </div>
            </div>
        </header>

        <div class="sidebar">
            <h4>MENÚ</h4>
            <ul>
                <li><a href="PanelRepresentanteV.jsp" style="font-size: 20px; color: white; ">Panel principal</a></li>
                <li><a href="PerfilRepresentante.jsp" style="font-size: 20px; color: white; ">Volver</a></li>
            </ul>
        </div>
        <div class="contenido">
            <img src="../images/Menu.png" alt="" class="menu-bar">
            <div class="col-md-12 main-section" style="margin: auto;">
                <div class="col-md-9 main-section">
                    <div class="modal-content text-center col-md-4" style="margin-left: 10%;">
                        <h3 style="text-align: left; color: blue; font-weight: bolder;" >Datos personales</h3>
                    </div>
                    <form   method="get" enctype="text/plain">
                        <div class="modal-content text-center " style="margin-left: 10%;">
                            <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                                <label for="inputNombre" class="col-form-label" style="text-align: right; font-weight: bolder;">Nombres</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" value="<%=oRep.getStrNombre()%>" name="txtNombre" id="txtNombre" >
                                </div>
                                <label for="inputApellido" class=" col-form-label" style="text-align: right; font-weight: bolder;">Apellidos</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control"  value="<%=oRep.getStrApellido()%>" name="txtApellido" id="txtApellido">
                                </div>
                            </div>
                            <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                                <label for="inputCedula" class=" col-form-label" style="text-align: right; font-weight: bolder;">Cédula</label>
                                <div class="col-sm-3" >
                                    <input type="text" class="form-control"  value="<%=oRep.getStrCedula()%>" name="txtCedula" id="txtCedula" readonly>
                                </div>
                                <label for="inputEdad" class=" col-form-label" style="text-align: right; font-weight: bolder;">Edad </label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control"  value="<%=oRep.getIntEdad()%>" name="txtEdad" id="txtEdad" >
                                </div>
                                <label for="inputNivelAcad" class="col-form-label" style="text-align: right; font-weight: bolder;">Nivel académico</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control"  value="<%=oRep.getStrNivelAca()%>"  name="txtNivelAcad" id="txtNivelAcad">
                                </div>
                            </div>
                            <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                                <label for="inputCorreo" class="col-form-label" style="text-align: right; font-weight: bolder;">Correo</label>
                                <div class="col-sm-5" Style="padding-left: 3.5%;">
                                    <input type="text" class="form-control"  value="<%=oRep.getStrCorreo()%>" name="txtCorreo" id="txtCorreo">
                                </div>
                                <label for="inputTelefono" class=" col-form-label" style="text-align: right; font-weight: bolder;">Teléfono</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control"  value="<%=oRep.getStrTelefono()%>" name="txtTelefono" id="txtTelefono">
                                </div>
                            </div>
                            <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >

                                <label for="inputCelular1" class="col-form-label" style="text-align: right; font-weight: bolder;">Celular 1</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control"  value="<%=oRep.getStrCelular1()%>" name="txtCelular1" id="txtCelular1">
                                </div>
                                <label for="inputCelular2" class=" col-form-label" style="text-align: right; font-weight: bolder;">Celular 2</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control"  value="<%=oRep.getStrCelular2()%>"  name="txtCelular2" id="txtCelular2">
                                </div>
                            </div>
                            <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >
                                <label for="inputDireccion" class="col-form-label" style="text-align: right; font-weight: bolder;">Dirección</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control"  value="<%=oRep.getStrDireccion()%>"  name="txtDireccion" id="txtDireccion">
                                </div>
                            </div>
                            <div class="form-group row" style="padding-top: 3%; padding-left: 4%; " >
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-3" style="text-align: right;">
                                    <button type="button" class="btn btn-secondary" onClick="history.back()">Cancelar</button>
                                </div>
                                <div class="col-sm-3" >
                                    <input type="button" name="btn_Guardar"  class="btn btn-primary" value="Guardar" id="btn_guardarDatos" onclick="guardarDatos();" >
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            $('.menu-bar').on('click', function () {
                $('.contenido').toggleClass('abrir');
            });

//            Guardar datos modificados
            function guardarDatos() {
                var nombreRep = document.getElementById('txtNombre').value;
                var apellidoRep = document.getElementById('txtApellido').value;
                var cedulaRp = document.getElementById('txtCedula').value;
                var edadRep = document.getElementById('txtEdad').value;
                var correoRep = document.getElementById('txtCorreo').value;
                var telefonoRep = document.getElementById('txtTelefono').value;
                var celular1Rep = document.getElementById('txtCelular1').value;
                var celular2Rep = document.getElementById('txtCelular2').value;
                var nivelAcaRp = document.getElementById('txtNivelAcad').value;
                var direccionRep = document.getElementById('txtDireccion').value;
//                console.log(nombreRep +' ' +apellidoRep+' ' +edadRep+' ' +correoRep+' ' +telefonoRep+' ' +celular1Rep+' ' +celular2Rep+' ' +direccionRep+' ' +nivelAcaRp );
                $.ajax({
                    url: "../Controladores/RepresentanteOpC.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: "modificarDatos",
                        nombreR: nombreRep,
                        apellidoR: apellidoRep,
                        cedulaR: cedulaRp,
                        edadR: edadRep,
                        correoR: correoRep,
                        telefonoR: telefonoRep,
                        celular1R: celular1Rep,
                        celular2R: celular2Rep,
                        nivelAcadR: nivelAcaRp,
                        direccionR: direccionRep
                    },
                    success: function (response) {
                        if (response!=="")
                        {
                            Swal.fire({
                                icon: 'success',
                                title: 'Éxito',
                                text: 'Datos Modificados'
                            })

                        } else {
                            alert("Error");
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'Se produjo un error'
                            })
                        }

                    },
                    error: function (error) {
                        console.log(error);
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Se produjo un error'
                        })
                    }
                });
            }
        </script>
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/sweetalert2.all.min.js"></script>

    </body>
</html>
