<%-- 
    Document   : EstudiantesV
    Created on : 29/11/2019, 10:35:25
    Author     : Angel
--%>

<%@page import="ggm.comun.evaluacion"%>
<%@page import="ggm.comun.estudiante"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ggm.comun.clase"%>
<%@page import="ggm.comun.paralelo"%>
<%@page import="ggm.comun.materia"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <!--<link rel="stylesheet" type="text/css" href="../css/vistadocente.css">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>Actividades & Tareas </title>
        <style>
            .table .thead-dark{

                color:#456789;
                font-size:10%;
            }
        </style>
    </head>
    <body>
        <header >
            <div class="page-header" style="background:#252932; padding-left: 20px; padding-right: 20px" >
                <div class="form-row">

                    <div class="col-md-2">
                        <a title="Logo 1" href="PanelDocenteV.jsp"><img class="center-block" src="../images/logo.jpg" width="55px" height="70px"
                                                                        style="margin-left: 40%; margin-top: 5%; margin-bottom: 3% "></a>
                    </div>
                    <div class="col-md-8">
                        <h1 style="width:100%; margin-left: 1%; margin-top:15px; color:#f2f2f2; "> Unidad Educativa "Dr. Gabriel García Moreno" </h1>
                    </div>
                    <div class="col-md-2">
                        <img class="center-block" src="../images/logo2.jpg" width="55px" height="70px"
                             style=" margin-top: 5%; margin-bottom: 3%; margin-left: 25%;">
                    </div>
                </div>
            </div>
        </header>
        <br>
        <div class="col-md-7 main-section">
            <div class="modal-content" style="margin-left: 35%;">
                <h2 style="text-align: center;">Lista de actividades académicas</h2>
            </div>
        </div>
        <br>
        <br>
        <div class="col-md-7 main-section">
            <div class="modal-content" style="margin-left: 35%;">

                <div style="text-align: center; margin-left: 12%;" id="contenidoDinamico" class='col-xs-9'>
                    <div class='form-group row'>
                        <div class='col-xs-12'>
                            <br>            
                            <table class="table table-bordered" id="tablaActividades">
                                <thead style="background: #abdde5 ">
                                    <tr>
                                        <th style="width: 3%;"></th>
                                        <td ><b>ID</b></td>
                                        <td ><b>NOMBRES</b></td>
                                        <td ><b>FECHA REALIZADO</b></td>
                                        <td ><b>FECHA PLAZO</b></td>
                                        <td ><b>TIPO</b></td>
                                        <th style="width: 3%;"></th>
                                    </tr>
                                </thead>
                                <tbody id="tablaActividadesDoc">
                                    <tr class="hide">
                                        <td style="width: 3%;"></td>
                                        <td  contenteditable="true"></td>
                                        <td  contenteditable="true"></td>
                                        <td  contenteditable="true"></td>
                                        <td  contenteditable="true"></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            $(document).ready(function listarActividades() {

                $.ajax({
                    url: "../Modelos/ActividadesM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarActividadesDoc'
                    },
                    success: function (resultado) {

                        var activiades = JSON.parse(resultado);
                        //                        alert(activiades)
                        var html = "";
                        activiades.forEach(function (actividad) {
                            html += "<tr>";
                            html += "<td></td>";
                            html += "<td>" + actividad.intIdActividad + "</td>";
                            html += "<td>" + actividad.strNombre + "</td>";
                            html += "<td>" + actividad.strFechaRealizado + "</td>";
                            html += "<td>" + actividad.strFechaPlazo + "</td>";
                            html += "<td>" + actividad.strTipo + "</td>";
                            html += "<td></td>";
                            html += "</tr>";

                        });
                        $('#tablaActividadesDoc').html(html);
                    },
                    error: function (error) {
                        onsole.log(error);
                    }
                });
            });

        </script>
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>

    </body>
</html>
