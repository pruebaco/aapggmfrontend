<%-- 
    Document   : EstudiantesV
    Created on : 29/11/2019, 10:35:25
    Author     : Angel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/vistaAdmi.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>Administrador</title>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark navbar-expand-lg sticky-top">

            <nav class="navbar navbar-dark bg-dark">
                <a class="navbar-brand" href="#">
                    <img src="../img/Escudo.ico" width="30" height="30" class="d-inline-block align-top" alt="">
                    Administrador
                </a>
            </nav>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuAdmonistrador" aria-controls="menuAdmonistrador" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="menuAdmonistrador">
                <ul class="nav nav-item mr-auto" style="text-decoration: white; ">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" style="color: #ffffff;">Inicio</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: #ffffff;">Datos personales</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="../Controladores/AdministrativoC.jsp?opcion=mostrarPerfil">Perfil</a>
                            <a class="dropdown-item" href="#">Modificar</a>

                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" style="color: #ffffff;">Reportes</a>
                    </li>

                </ul>
                <div>
                    <a class="nav-link" href="../index.jsp" style="color: #ffffff;">Cerrar Sesión </a>
                </div>
            </div>
        </nav>
        <div class="modal-content text-center col-md-4" style="margin-left: 34%; margin-bottom: -0.3%; margin-top: 6%;">
            <h3 style="text-align: left; color: blue; font-weight: bolder;" >Bienvenido administrador</h3>
        </div>
        <div class="col-lg-10 main-section">
            <div class="modal-content" >
                <div  id="contenidoDinamico">
                    <!--Mision vision-->
                </div>
            </div>
        </div>
        <div class="footer" >
            Unidad Educativa Dr. Gabriel García Moreno
            <br>
            Guano - Riobamba
            <br>
            2020 - 2021
        </div>

        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/Mision-Vision.js"></script>
    </body>
</html>
