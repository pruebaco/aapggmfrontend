<%-- 
    Document   : PerfilDocenteV
    Created on : 13/11/2019, 19:15:33
    Author     : Angel
--%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.docente"%>
<%@page session="true" %>
<%
    docente oDoc = new docente();
    Gson oGson = new Gson();

    HttpSession sesion = request.getSession(true);
    String datosDocente = sesion.getAttribute("datosDocente").toString();
    oDoc = oGson.fromJson(datosDocente, docente.class);

    System.out.println("Perfil del docente EN LA VISTA " + datosDocente);


%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../css/RvistaPerfil.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <title>Perfil Docente</title>
    </head>
    <body>

        <main>

            <div class="container">
                <div class="img-titulo">
                    <img class="img-fluid" src="../img/titulo.jpg">    
                </div>

                <div id="content-contenido" class="col-12">
                    <div id="barra-superior" class="form-group row">
                        <h3 style="text-align: center; color: white;"> &nbsp;&nbsp;&nbsp;&nbsp; Perfil del Docente </h3>
                    </div>
                    <div id="datos" style="overflow:scroll;">
                        <!--Envio de datos para ser modificados--> 
                        <form action="Dmodificar.jsp" method="get" enctype="text/plain">
                            <div id="content-right" class="col-8" >

                                <br>
                                <h3 style="text-align: center"> Datos personales </h3> <br>

                                <div class="form-group row">
                                    <label for="inputNombre" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Nombres</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" value="<%=oDoc.getStrNombre()%>" name="txtNombre" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputApellido" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Apellidos</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrApellido()%>" name="txtApellido"readonly >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputCedula" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Cédula</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrCedula()%>" name="txtCedula" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEdad" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Edad</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getIntEdad()%>" name="txtEdad" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputCorreo" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Correo</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrCorreo()%>" name="txtCorreo" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputTelefono" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Teléfono</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrTelefono()%>" name="txtTelefono" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputCelular1" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Celular 1</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrCelular1()%>" name="txtCelular1" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputCelular2" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Celular 2</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrCelular2()%>"  name="txtCelular2" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputTitulo1" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Titulo 1</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrTitulo1() %>"  name="txtTitulo1" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputTitulo2" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Titulo 2</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrTitulo2() %>"  name="txtTitulo2" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputTitulo3" class="col-sm-3 col-form-label" style="color: blue;text-align: right;">Titulo 3</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"  value="<%=oDoc.getStrTitulo3() %>"  name="txtTitulo3" readonly>
                                    </div>
                                </div>
                            </div>
                            <div id="content-left" class="col-2">
                                <br> 
                                <h3 style="text-align: center"> Perfil </h3>
                                <div id="Foto-perfil" class="col-12 user-img">
                                    <img src="../img/user.png" alt="Icon" class="img-fluid">

                                </div>
                                <br> 
                                <div class="col-12 forgot">
                                    <button type="submit" class="btn btn-success btn lg" id="btnModificar" class="centrado" > Modificar </button>
                                    <br>
                                    <br>
                                    <br>
                                    <button type="button" class="btn btn-danger btn lg" id="btnAtras" class="centrado" onClick="document.action = 'revistas_limpiar.php'" >Atrás</button>
                                </div>
                            </div>
                        </form>  
                    </div>
                </div>

            </div>
        </main>
        <script src="js/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>

    </body>
</html>