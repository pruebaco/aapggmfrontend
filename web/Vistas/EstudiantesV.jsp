<%-- 
    Document   : EstudiantesV
    Created on : 29/11/2019, 10:35:25
    Author     : Angel
--%>

<%@page import="ggm.comun.evaluacion"%>
<%@page import="ggm.comun.estudiante"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ggm.comun.clase"%>
<%@page import="ggm.comun.paralelo"%>
<%@page import="ggm.comun.materia"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>

<%

    Gson oGson = new Gson();
    JsonArray array;
    array = null;
    try {
        String dClase = session.getAttribute("dClase").toString();
        JsonParser objetoM = new JsonParser();
        JsonElement objetoL = objetoM.parse(dClase); //Para convertir de string a Json
        array = objetoL.getAsJsonArray();//Convertir de un elemento a un Array 

        for (int i = 0; i < array.size(); i++) {

            JsonElement objetoElemento = array.get(i); //Extrae uno x uno los elementos
            JsonObject objetoGeneral = objetoElemento.getAsJsonObject(); // El elemento json se convierte en objeto json
            materia objMateria = oGson.fromJson(objetoGeneral.get("materia").getAsString(), materia.class); //El objeto json en una clase materia
            paralelo objParalelo = oGson.fromJson(objetoGeneral.get("paralelo").getAsString(), paralelo.class); //El objeto json en una clase paralelo
            clase objClase = oGson.fromJson(objetoGeneral.get("clase").getAsString(), clase.class); //El objeto json en una clase Clase
//        System.out.println("NUEVO JSON: " + objClase.getIntIdClase() + " " + objMateria.getStrNombreMAteria() + " " + objParalelo.getStrParalelo());

        }
    } catch (Exception e) {
        System.out.println("Excepción Catch  Vista " + e.toString());
    }
%>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
        <!--<link rel="stylesheet" type="text/css" href="../css/vistadocente.css">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>Estudiantes</title>
        <style>
            .table .thead-dark{

                color:#456789;
                font-size:10%;
            }
        </style>
    </head>
    <body>
        <header >
            <div class="page-header" style="background:#252932; padding-left: 20px; padding-right: 20px" >
                <div class="form-row">

                    <div class="col-md-2">
                        <a title="Logo 1" href="PanelDocenteV.jsp"><img class="center-block" src="../images/logo.jpg" width="27.5px" height="35px"
                                                                        style="margin-left: 40%; margin-top: 5%; margin-bottom: 3% "></a>
                    </div>
                    <div class="col-md-8">
                        <h3 style="  text-align:center; margin-left: auto; margin-right: auto; margin-top:15px; color:#f2f2f2; "> Unidad Educativa "Dr. Gabriel García Moreno" </h3>
                    </div>
<!--                    <div class="col-md-2">
                        <img class="center-block" src="../images/logo2.jpg" width="27.5px" height="35px"
                             style=" margin-top: 5%; margin-bottom: 3%; margin-left: 25%;">
                    </div>-->
                </div>
            </div>
        </header>
        <br>
        <div class="col-md-8 main-section">
            <div class="modal-content" style="margin-left: 25%;">
                <h2 style="text-align: center;">Gestión de notas de estudiantes</h2>
                <div class="form-row" style="margin-top: 3%; ">
                    <div class="col-sm-3 mb-3" style="font-weight: bold; margin-left: 15%;">
                        <label for="inputState"> &nbsp;Paralelos </label>
                        <select onChange="recuperarMaterias();" name="paralelos" id="opcionesParalelosDoc" class="custom-select"  required=""> 
                            <option  value="" >Seleccionar paralelo</option>
                            <%
                                int antes = 0;
                                if (array != null) {
                                    for (int i = 0; i < array.size(); i++) {

                                        JsonElement objetoElemento = array.get(i); //Extrae uno x uno los elementos
                                        JsonObject objetoGeneral = objetoElemento.getAsJsonObject(); // El elemento json se convierte en objeto json
//                                    materia objMateria = oGson.fromJson(objetoGeneral.get("materia").getAsString(), materia.class); //El objeto json en una clase materia
                                        paralelo objParalelo = oGson.fromJson(objetoGeneral.get("paralelo").getAsString(), paralelo.class); //El objeto json en una clase paralelo
                                        clase objClase = oGson.fromJson(objetoGeneral.get("clase").getAsString(), clase.class); //El objeto json en una clase Clase
                                        if ((i > 0)) {
                                            if (antes != objParalelo.getIntIdParalelo()) {
                                                out.print(" <option  value = '" + objParalelo.getIntIdParalelo() + "' > " + objParalelo.getStrParalelo() + "</option >");
                                                antes = objParalelo.getIntIdParalelo();
                                            }

                                        } else {
                                            antes = objParalelo.getIntIdParalelo();
                                            out.print(" <option  value = '" + objParalelo.getIntIdParalelo() + "' > " + objParalelo.getStrParalelo() + "</option >");
                                        }

                                    }
                                }
                            %>
                        </select>
                    </div>
                    <div class="col-sm-3 mb-3" style="font-weight: bold; margin-left: 20%;">
                        <label for="inputState"> &nbsp;Materia</label>
                        <select name="materias" id="opcionesMateriasDoc" class="custom-select" required > 
                            <option value="">Seleccionar materia </option>

                        </select>
                    </div>
                </div>
                <div class="form-row" style="margin-top: 3%; ">
                    <div class="col-sm-3 mb-3" style="font-weight: bold; margin-left: 15%; ">
                        <label for="inputState"> &nbsp;Quimestre</label>
                        <select name="quimestre" id="opcionesQuimestres" class="custom-select" required > 
                            <option value="">Seleccionar quimestre</option>
                            <option value="1">Primer quimestre</option>
                            <option value="2">Segundo quimestre</option>
                        </select>
                    </div>
                    <div class="col-sm-3 mb-3" style="font-weight: bold; margin-left: 20%; ">
                        <label for="inputState"> &nbsp;Parcial</label>
                        <!--<select onChange="recuperarMaterias();" name="paralelos" id="opcionesParalelosDoc" class="custom-select"  required="">--> 
                        <select onChange="listarEstudiantes();" name="parciales" id="opcionesParcial" class="custom-select" required > 
                            <option value="">Seleccionar parcial</option>
                            <option value="1">Primer parcial</option>
                            <option value="2">Segundo parcial</option>
                            <option value="3">Tercer parcial</option>
                        </select>
                    </div>
                </div>
            </div>
            <!--<input type="button" id="btn_guardar" name="btn_guardar"  class="btn btn-primary" value="listar" onclick="listarEstudiantes();"  >-->
            <br>
            <div style="text-align: center; margin-left: 15%;" id="contenidoDinamico" class='col-xs-9'>
                <div class='form-group row'>
                    <div class='col-xs-12'>
                        <table class="table table-bordered" id="tablaPacientes">
                            <thead style="background: #abdde5">
                                <tr>
                                    <th style="width: 3%;"></th>
                                    <td ><b>C&Eacute;DULA</b></td>
                                    <td ><b>NOMBRES</b></td>
                                    <td ><b>APELLIDOS</b></td>
                                    <td ><b>TAREAS INDIVIDUALES</b></td>
                                    <td ><b>TAREAS GRUPALES</b></td>
                                    <td ><b>DEBERES</b></td>
                                    <td ><b>EXAMEN(ES)</b></td>
                                    <td ><b>PROMEDIO</b></td>
                                    <td colspan="2"><b>ACCI&Oacute;N</b></td>
                                </tr>
                            </thead>
                            <tbody id="tablaEstudianteNotas">
                                <tr class="hide">
                                    <td style="width: 3%;"></td>
                                    <td  contenteditable="true"></td>
                                    <td  contenteditable="true"></td>
                                    <td  contenteditable="true"></td>
                                    <td  contenteditable="true"></td>
                                    <td  contenteditable="true"></td>
                                    <td  contenteditable="true"></td>
                                    <td  contenteditable="true"></td>
                                    <td  contenteditable="true"></td>
                                    <td> </td>
                                    <td> </td>

                                    <!--<input type="hidden" value="3" />-->
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">

            function recuperarMaterias() {
                var ParalMat = document.getElementById('opcionesParalelosDoc').value;
                $.ajax({
                    url: "../Modelos/EstudianteM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'recuperarMaterias', idParalelo: ParalMat
                    },
                    success: function (resultado) {
                        var materias = JSON.parse(resultado);
                        var html = "";
                        materias.forEach(function (materia) {
                            html += "<option value='" + materia.intIdMAteria + "'>" + materia.strNombreMAteria + "</option>";
                        });
                        $('#opcionesMateriasDoc').html(html);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
//
            function listarEstudiantes() {
                var ParalMat = document.getElementById('opcionesParalelosDoc').value;
                var MateriaMat = document.getElementById('opcionesMateriasDoc').value;
                var QuimestreEst = document.getElementById('opcionesQuimestres').value;
                var ParcialEst = document.getElementById('opcionesParcial').value;
                $.ajax({
                    url: "../Modelos/EstudianteM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listarEstudiantes', paraleloMat: ParalMat, materiaEst: MateriaMat, quimestreEst: QuimestreEst, parcialEst: ParcialEst
                    },
                    success: function (resultado) {
                        console.log(resultado)
                        var estudiantes = JSON.parse(resultado);
                        var html = "";
                        estudiantes.forEach(function (estudiante) {
                            html += "<tr>";
                            html += " <input type='hidden' value='" + estudiante.idEvaluacion + "'>";
                            html += "<td></td>";
                            html += "<td>" + estudiante.cedula + "</td>";
                            html += "<td>" + estudiante.nombre + "</td>";
                            html += "<td>" + estudiante.apellido + "</td>";
                            html += "<td>" + estudiante.nota1 + "</td>";
                            html += "<td>" + estudiante.nota2 + "</td>";
                            html += "<td>" + estudiante.nota3 + "</td>";
                            html += "<td>" + estudiante.nota4 + "</td>";
                            html += "<td>" + ((parseFloat(estudiante.nota1) + parseFloat(estudiante.nota2) + parseFloat(estudiante.nota3) + parseFloat(estudiante.nota4)) / 4) + "</td>";
                            html += "<td><i onclick='notas(this);' class='far fa-edit' style='font-size:24px; color:blue; cursor: auto'></i></td>";
                            html += "<td><i onclick='guardar_notas(this);' class='far fa-save' style='font-size:24px; color:blue; cursor: auto '></i></td>";
                            html += " <input type='hidden' value='" + estudiante.edad + "'>";
                            html += "</tr>";

                        });
                        $('#tablaEstudianteNotas').html(html);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            function notas(event) {
                for (var i = 6; i < 10; i += 1) {
                    event.parentNode.parentNode.childNodes[i].innerHTML = "<input type='number' class='form-control' min='0' max='10' step='0.1' value='" + event.parentNode.parentNode.childNodes[i].textContent + "'>";
                }
            }

            function guardar_notas(event) {
                var ParalMat = document.getElementById('opcionesParalelosDoc').value;
                var MateriaMat = document.getElementById('opcionesMateriasDoc').value;
                var QuimestreEst = document.getElementById('opcionesQuimestres').value;
                var ParcialEst = document.getElementById('opcionesParcial').value;
                var array = [];
                var sum = 0;
                for (var i = 6; i < 10; i += 1) {
                    sum += parseFloat(event.parentNode.parentNode.childNodes[i].childNodes[0].value);
                    array.push(parseFloat(event.parentNode.parentNode.childNodes[i].childNodes[0].value));
                    event.parentNode.parentNode.childNodes[i].textContent = event.parentNode.parentNode.childNodes[i].childNodes[0].value;
                }
                event.parentNode.parentNode.childNodes[10].textContent = sum / 4;
                var id_evaluacion = event.parentNode.parentNode.firstElementChild.value;
                var id_estudiante = event.parentNode.parentNode.lastElementChild.value;
//                var id_evaluacion = event.parentNode.parentNode.childNodes[11].textContent;

                console.log(' Estudiante: ' + id_estudiante);
                console.log(' Evaluacion: ' + id_evaluacion);
                var notas = JSON.stringify(array);

                $.ajax({
                    url: "../Controladores/EstudiantesC.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {opcion: "guardarNotas", notasEstudiante: notas, paraleloMat: ParalMat, materiaEst: MateriaMat, quimestreEst: QuimestreEst, parcialEst: ParcialEst, idEstudiante: id_estudiante, idEvaluacion: id_evaluacion},
                    success: function (response) {

                    },
                    error: function (error) {
                        console.log('Error ' + error);
                    }
                });
            }

        </script>

        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.bundle.min.js"></script>

    </body>
</html>
