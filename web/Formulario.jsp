<%-- 
    Document   : Representante
    Created on : 04/05/2019, 14:49:59
    Author     : Angel
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/Prueba.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <title>Formularios</title>
        <!--Inico de Botones dinamicos-->

        <!--        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" media="screen">
                <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
        <script type="text/javascript">
            $(document).ready(function () {
                var maxFieldLimit = 2; //Input fields increment limitation
                var add_more_button = $('.add_button'); //Add button selector
                var Fieldwrapper = $('.input_field_wrapper'); //Input field wrapper
                var fieldHTML = '<div><div class="form-row"> <label for="inputState">Nuevo paralelo </label> &nbsp; <input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"> &nbsp; <i class="fas fa-minus-circle" style="font-size:25px"; float: left;></i></a></div></div>'; //New input field html 

                var x = 1; //Initial field counter is 1
                $(add_more_button).click(function () { //Once add button is clicked
                    if (x < maxFieldLimit) { //Check maximum number of input fields
                        x++; //Increment field counter
                        $(Fieldwrapper).append(fieldHTML); // Add field html
                    }
                });
                $(Fieldwrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
                    e.preventDefault();
                    $(this).parent('div').remove(); //Remove field html
                    x--; //Decrement field counter
                });
            });
        </script>
        <!--Fin de Botones dinamicos-->
    </head>
    <body>
        <!--
        Botones de las modales de registros
                <input type="button" name="btn_registro" class="btn btn-success" data-toggle="modal" data-target=".modal-estudiante" value="Registro Usuario" id="btn_modalRegistro">
                <input onclick="loadModal(); return false" type="button" name="btn_registro" class="btn btn-success" data-toggle="modal" value="Registro Usuario" id="btn_modalRegistro">
                <br>
                <br>
                <button type="button" id="btn_modalRegistro" class="btn btn-success" data-toggle="modal" data-target=".modal-estudiante">Registro </button>
                Modal para registar Usuarios 
                <div class="modal fade modalRegistro" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form class="needs-validation" novalidate>
                                <div class="modal-header" style="background: #004050; color: white;">
                                    <h5 class="modal-title" id="exampleModalLabel" style="text-align: center;" >REGISTRO DE USUARIOS</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <br>
                                                            <form class="needs-validation" novalidate>
        
                                    <div class="form-row">
                                        <div class="col-md-5 mb-4">
                                            <label for="validationCustom01">Nombres</label>
                                            <input type="text" class="form-control" id="validationCustom01" name="nombre" placeholder="Nombres" value="Luis" required>
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <label for="validationCustom02">Apellidos</label>
                                            <input type="text" class="form-control" id="validationCustom02" name="apellido" placeholder="Apellidos" value="Ortiz" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-5">
                                            <label for="inputState">Periodo académico</label>
                                            <select name="peridoAca" id="opcionesPeriodos" class="custom-select" required> 
                                                <option value="">Abrir el menú para seleccionar</option>
                                            </select>
                                            <div class="invalid-feedback">Seleccione el periodo académico</div>
                                        </div>
        
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom02">Cedula</label>
                                            <input type="text" class="form-control" name="cedula" id="validationCustom02" placeholder="110434423-0" required>
                                            <div class="invalid-feedback">Ingrese la cedula</div>
                                        </div>
        
                                        <div class="form-group col-md-2 mb-4">
                                            <label for="inputState">Edad</label>
                                            <input type="number" class="form-control" name="edad" id="validationCustom02" placeholder="Edad" min="18"  max="60" required>
                                            <div class="invalid-feedback">Ingrese la edad</div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-5 mb-4">
                                            <label for="exampleInputEmail1">Correo</label>
                                            <input type="email" class="form-control" name="correo" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese correo electrónico" required>
                                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                            <div class="invalid-feedback">Ingrese una dirección de correo válida</div>
                                        </div>
                                        <div class="form-group col-md-6 mb-4">
                                            <label for="exampleInputPassword1">Contraseña</label>
                                            <input type="password" class="form-control" name="contrasena" id="exampleInputPassword1" placeholder="Password" required>
                                            <div class="invalid-feedback">Ingrese una contraseña</div>
                                        </div>
        
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-2 mb-2">
                                            <label for="inputState">Tipo </label>
                                        </div>
                                        <div class="custom-control custom-radio col-md-3 mb-4">
                                            <input type="radio" class="custom-control-input" id="customControlValidation2" name="tipo" required>
                                            <label class="custom-control-label" for="customControlValidation2">Docente</label>
                                        </div>
                                        <div class="custom-control custom-radio col-md-3 mb-4">
                                            <input type="radio" class="custom-control-input" id="customControlValidation3" name="tipo" required>
                                            <label class="custom-control-label" for="customControlValidation3">Representante</label>
                                        </div>
        
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-3 mb-4">
                                            <label >Telefono</label>
                                            <input type="text" class="form-control" name="telefono" placeholder="(3)900-XXXX"  >
        
                                        </div>
                                        <div class="col-md-3 mb-4">
                                            <label for="validationCustom02">Celular 1</label>
                                            <input type="text" class="form-control"  name="celular1"  id="validationCustom02" placeholder="0983878499"  required>
                                            <div class="invalid-feedback">Ingrese un número</div>
                                        </div>
                                        <div class="col-md-3 mb-4">
                                            <label >Celular 2</label>
                                            <input type="text" class="form-control" name="celular2" id="validationCustom02" placeholder="0983878445"  >
        
                                        </div>
                                    </div>
        
                                    <button class="btn btn-primary" type="submit">Submit form</button>
                                    </form>
        
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </form>
        
                            <script>
                                // Example starter JavaScript for disabling form submissions if there are invalid fields
                                (function () {
                                    'use strict';
                                    window.addEventListener('load', function () {
                                        // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                        var forms = document.getElementsByClassName('needs-validation');
                                        // Loop over them and prevent submission
                                        var validation = Array.prototype.filter.call(forms, function (form) {
                                            form.addEventListener('submit', function (event) {
                                                if (form.checkValidity() === false) {
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                }
                                                form.classList.add('was-validated');
                                            }, false);
                                        });
                                    }, false);
                                })();
                            </script>
        
                        </div>
                    </div>
                </div>
                Fin de la Modal para registar Usuarios 
                <br>
        
                 Modal de Registro de usuarios 
                <input onclick="loadModal(); return false" type="button" name="btn_registro" class="btn btn-success" data-toggle="modal" value="Registro Usuario" id="btn_modalRegistro">
                <button onclick="loadModal(); return false" type="button" class="btn btn-primary" data-toggle="modal" data-target=".modalRegistro" id="btn_modalRegistro"> Registro </button>
        
                <div class="modal fade modalRegistro" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header" style="background: #004050; color: white;">
                                <h5 class="modal-title" id="exampleModalLabel" style="text-align: center;" >REGISTRO DE USUARIOS</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
        
                                <form class="needs-validation" novalidate>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-4">
                                            <label for="validationCustom01">Nombres</label>
                                            <input type="text" class="form-control" id="validationCustom01" placeholder="Nombres" value="Luis" required>
                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <label for="validationCustom02">Apellidos</label>
                                            <input type="text" class="form-control" id="validationCustom02" placeholder="Apellidos" value="Ortiz" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-5">
                                            <label for="inputState">Period académico</label>
                                            <select id="opcionesPeriodos"class="custom-select" required>
                                                <option value="">Abrir el menú para seleccionar</option>
                                            </select>
                                            <div class="invalid-feedback">Seleccione el periodo académico</div>
                                        </div>
        
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom02">Cedula</label>
                                            <input type="text" class="form-control" id="validationCustom02" placeholder="110434423-0" required>
                                            <div class="invalid-feedback">Ingrese la cedula</div>
                                        </div>
        
                                        <div class="form-group col-md-2 mb-4">
                                            <label for="inputState">Edad</label>
                                            <input type="number" class="form-control" id="validationCustom02" placeholder="Edad" min="18"  max="60" required>
                                            <div class="invalid-feedback">Ingrese la edad</div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-5 mb-4">
                                            <label for="exampleInputEmail1">Correo</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese correo electrónico" required>
                                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                            <div class="invalid-feedback">Ingrese una dirección de correo válida</div>
                                        </div>
                                        <div class="form-group col-md-6 mb-4">
                                            <label for="exampleInputPassword1">Contraseña</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
                                            <div class="invalid-feedback">Ingrese una contraseña</div>
                                        </div>
        
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-2 mb-2">
                                            <label for="inputState">Tipo </label>
                                        </div>
                                        <div class="custom-control custom-radio col-md-3 mb-4">
                                            <input type="radio" class="custom-control-input" id="customControlValidation2" name="radio-stacked" required>
                                            <label class="custom-control-label" for="customControlValidation2">Docente</label>
                                        </div>
                                        <div class="custom-control custom-radio col-md-3 mb-4">
                                            <input type="radio" class="custom-control-input" id="customControlValidation3" name="radio-stacked" required>
                                            <label class="custom-control-label" for="customControlValidation3">Representante</label>
                                            <div class="invalid-feedback">More example invalid feedback text</div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-3 mb-4">
                                            <label >Telefono</label>
                                            <input type="text" class="form-control"  placeholder="(3)900-XXXX"  >
        
                                        </div>
                                        <div class="col-md-3 mb-4">
                                            <label for="validationCustom02">Celular 1</label>
                                            <input type="text" class="form-control" id="validationCustom02" placeholder="0983878499"  required>
                                            <div class="invalid-feedback">Ingrese un número</div>
                                        </div>
                                        <div class="col-md-3 mb-4">
                                            <label >Celular 2</label>
                                            <input type="text" class="form-control" id="validationCustom02" placeholder="0983878445"  >
        
                                        </div>
                                    </div>
        
                                    <button class="btn btn-primary" type="submit">Submit form</button>
                                </form>
        
                                <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function () {
                                        'use strict';
                                        window.addEventListener('load', function () {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function (form) {
                                                form.addEventListener('submit', function (event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    form.classList.add('was-validated');
                                                }, false);
                                            });
                                        }, false);
                                    })();
                                </script>
        
        
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="button" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
        
                <script type="text/javascript">
        
                    function loadModal() {
                        $.ajax({
                            url: "./login/registromodelo.jsp",
                            type: 'GET',
                            dataType: 'text',
                            data: {
                                accion: 'listadoPeriodos'
                            },
                            success: function (resultado) {
                                var periodos = JSON.parse(resultado);
                                var html = "";
                                periodos.forEach(function (periodo) {
        //                         var html0 = "<option value='" + opciones+"'>"+ Abrir el menú para seleccionar +  "</option>";
        
                                    html += "<option value='" + periodo.strPeriodoAcademico + "'>" + periodo.strPeriodoAcademico + "</option>";
                                });
                                $('#opcionesPeriodos').html(html);
                                $('#modalRegistro').modal('show');
                                console.log(resultado);
                            },
                            error: function (error) {
                                console.log(error);
                            }
                        });
                    }
        
                    $(document).ready(function () {
                        var maxField = 2; //Input fields increment limitation
                        var addButton = $('.add_button'); //Add button selector
                        var wrapper = $('.field_wrapper'); //Input field wrapper
                        var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><i class="fa fa-minus-square-o" style="font-size:25px"; float: left;></i></a></div>'; //New input field html 
                        var x = 1; //Initial field counter is 1
                        $(addButton).click(function () { //Once add button is clicked
                            if (x < maxField) { //Check maximum number of input fields
                                x++; //Increment field counter
                                $(wrapper).append(fieldHTML); // Add field html
                            }
                        });
                        $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
                            e.preventDefault();
                            $(this).parent('div').remove(); //Remove field html
                            x--; //Decrement field counter
                        });
                    });
                </script>-->

        <!--Inico de Botones dinamicos-->

        <div class="panel panel-primary">
            <h3> Importar la lista de estudiantes </h3>
            <br>
            <div class="panel-body">   
                <div class="input_field_wrapper">
                    <div>
                        <div class="form-row">
                            <label for="inputState">Paralelos </label>
                            <div class="form-group col-md-3">
                                <select name="field_name[]" id="opcionesPeriodos" class="custom-select" value="" > 
                                    <option value="">Abrir el menú para seleccionar</option>
                                </select>
                            </div>
                            <a href="javascript:void(0);" class="add_button" title="Add field">  <i class='fas fa-plus-square' style='font-size:24px'></i></a>
                        </div>
                    </div>
                </div>
                <!--inicio reportes-->
                <div class="ed-item web-100 main-center cross-center">

                    <a id="btn_pdf" style="text-decoration: none; color: black" href="Reportes/Reportepdfs.jsp" target="_blank"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a>
                    <input type="button" name="btn_pdf" value="PDF" id="btn_guardar">

                </div>
                <!--fin-->
                <div id="buttondiv">
                    <input type="file" id="inputfile" />
                    <input type="button" id="viewfile" class="bn alert-heading" value="Import file" />
                    <br><br>
                </div>
                <div class="container" id="container" >
                    <table class="table table-bordered" id="tableMain" >
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Cédula</th>
                                <th scope="col">Apellidos</th>
                                <th scope="col">Nombres</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--Fin de Botones dinamicos-->
        <!--opciones-->
        <!--<script src="js/jquery-3.3.1.slim.min.js"></script>-->
        <script src="/js/jquery-3.3.1.min.js"></script>
        <script src="/js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="../Reportes/reportepdf.js"></script>
        <!--<script src="docente.js"></script>-->
    </body>
</html>

