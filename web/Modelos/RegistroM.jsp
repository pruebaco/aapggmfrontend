<%-- 
    Document   : RegistroM
    Created on : 13/11/2019, 18:23:21
    Author     : Angel
--%>

<%
    String opcion = request.getParameter("opcion"); //Opciones
    String dUsuario = request.getParameter("dUsuario"); //Datos del usuario para ser registrado
    String dPeriodo = request.getParameter("dPeriodo"); //Datos del usuario para ser registrado

    try {

        if (opcion.equals("listadoPeriodosAca")) {

            serviciosweb.PeriodoacademicoWS_Service service = new serviciosweb.PeriodoacademicoWS_Service();
            serviciosweb.PeriodoacademicoWS port = service.getPeriodoacademicoWSPort();
            String listaPeriodosAca = port.listarPeriodoAcademico();
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(listaPeriodosAca);

        } else if (opcion.equals("registrarUsuarios")) {

            serviciosweb.SesionWS_Service service = new serviciosweb.SesionWS_Service();
            serviciosweb.SesionWS port = service.getSesionWSPort();
            String jsonUsuario = port.registroUsuario(dUsuario, dPeriodo);
            System.out.println("Estatus: " +jsonUsuario);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(jsonUsuario);
            
        }

    } catch (Exception e) {
        System.out.println("Excepci�n Catch  RegistroM " + e.toString());
    }
%>
