<%-- 
    Document   : PeriodoAca
    Created on : 25/11/2019, 16:42:44
    Author     : Angel
--%>

<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="ggm.comun.estudiante"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.paralelo"%>
<%@page session="true" %>
<%
    String opcion = request.getParameter("opcion"); //Opciones
    String opParal = request.getParameter("opParal"); //Data del paralelo

    try {
        if (opcion.equals("listarParalelosRep")) {

            serviciosweb.MateriaWS_Service service = new serviciosweb.MateriaWS_Service();
            serviciosweb.MateriaWS port = service.getMateriaWSPort();
            String paralelosEst = port.paralelosDeDocenteSinMateria();

            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(paralelosEst);
        } else if (opcion.equals("listarEstudiantesRep")) {
            System.out.println("Listo para listar ");
            paralelo oParalelo = new paralelo();
            Gson oGson = new Gson();
            oParalelo.setIntIdParalelo(Integer.parseInt(opParal));
            String dParalelo = oGson.toJson(oParalelo);

            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String ListarEst = port.listarEstudiantesSinRepresentante(dParalelo);

            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(ListarEst);

        } else if (opcion.equals("asignarEstudiantesRep")) {
            String jsonEst = request.getParameter("jsonEst");
//            recuperar la sesion del representante
            String datosRepresentante = session.getAttribute("datosRepresentante").toString();

            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String jsonEstudiante = port.asignarRepresentanteEstudiante(datosRepresentante, jsonEst);

            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(jsonEstudiante);

        } else if (opcion.equals("listarRepresentados")) {
            //            recuperar la sesion del representante
            String datosRepresentante = session.getAttribute("datosRepresentante").toString();
            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String jsonRepresentados = port.listadoEstudiantePorRepresentante(datosRepresentante);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(jsonRepresentados);
        } else if (opcion.equals("guardarDatosM")) {
            String datosEstudiante = request.getParameter("datosEstudiante");
            String idEstudiante = request.getParameter("idEstudiante");
            String cedulaEst = request.getParameter("cedulaEst");

            Gson oGson = new Gson();
            estudiante objEstudiante = new estudiante();

            JsonParser objetoEst = new JsonParser();
            JsonElement objetoL = objetoEst.parse(datosEstudiante); //Para convertir de string a Json
            JsonArray array = objetoL.getAsJsonArray();//Convertir de un elemento a un Array 
            System.out.println("Datos array:" + array);
            
            objEstudiante.setIntIdEstudiante(Integer.parseInt(idEstudiante));
            objEstudiante.setStrCedula(cedulaEst);
            objEstudiante.setStrNombres(array.get(0).getAsString());
            objEstudiante.setStrApellidos(array.get(1).getAsString());
            objEstudiante.setIntEdad(Integer.parseInt(array.get(2).toString()));
            objEstudiante.setStrTelefonoReferencia(array.get(3).getAsString());
                    
            String jsonEstudiante = oGson.toJson(objEstudiante);

            System.out.println("Datos json" + jsonEstudiante);
            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String dataEstudianteM = port.modificarEstudiante(jsonEstudiante);

            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(dataEstudianteM);
            System.out.println("Respuesta: " + dataEstudianteM);
        }

    } catch (Exception e) {
        System.out.println("Excepci�n Catch  RepresentadoM " + e.toString());
    }
%>