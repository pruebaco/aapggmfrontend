<%-- 
    Document   : ModeloParalelo
    Created on : 13/11/2019, 18:14:16
    Author     : Angel
--%>
<%@page import="ggm.comun.administrativo"%>
<%@page import="com.google.gson.Gson"%>
<%@page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String opcion = request.getParameter("opcion");
    String datos = request.getParameter("datos");
    Gson oGson = new Gson();
    administrativo oAdmi = new administrativo();

    try {
        if (datos != null) {
//  Obtiene los datos del Administrador

            Administrador.AdministrativoWS_Service service = new Administrador.AdministrativoWS_Service();
            Administrador.AdministrativoWS port = service.getAdministrativoWSPort();
            String dataAdm = port.perfilAdministrativo(datos);
            oAdmi = oGson.fromJson(dataAdm, administrativo.class);

            response.sendRedirect("../Controladores/AdministrativoC.jsp?dataAdm=" + dataAdm);

        } else if (opcion.equals("modificarDatos")) {

            String dataModificarAdm = request.getParameter("dataModificarAdm");

            Administrador.AdministrativoWS_Service service = new Administrador.AdministrativoWS_Service();
            Administrador.AdministrativoWS port = service.getAdministrativoWSPort();

            String dataAdmM = port.modificarAdministrativo(dataModificarAdm);

            if (dataAdmM.equals("1")) {
                String datosAct = port.perfilAdministrativo(dataModificarAdm);
                oAdmi = oGson.fromJson(datosAct, administrativo.class);
                String opcionAct = "datosActualizados";
                response.sendRedirect("../Controladores/AdministrativoC.jsp?datosAct=" + datosAct + "&opcion=" + opcionAct);
            }
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write("Exito");
            System.out.println("Respuesta:  " + dataAdmM);
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  RepresentanteM");
    }
%>
