<%-- 
    Document   : ParaleloM
    Created on : 19/11/2019, 12:03:02
    Author     : Angel
--%>

<%@page import="ggm.comun.docente"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.paralelo"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String accion = request.getParameter("accion"); //Recibe datos de los paralelo
//    String opcion = request.getParameter("opcion"); //Recibe datos de los paralelo
    System.out.println("Opcion:  " + accion);
    String opParal = request.getParameter("opParal"); //Data del paralelo
    String dparalelo = request.getParameter("dparalelo"); //Data del paralelo
    String listaEst = request.getParameter("dLista"); //Data de la lista de estudiantes
    System.out.println("Id de paralelo:   " + opParal);

    Gson objetoGson = new Gson();
    try {
        if (dparalelo != null) {
            System.out.println("1");
//            Recupera la sesion del docente con los datos 
            String datosDocente = session.getAttribute("datosDocente").toString();
//        Ingresar nuevos paralelos 
            System.out.println("Datos recuperados del docente " + datosDocente);
            serviciosweb.ParaleloWS_Service service = new serviciosweb.ParaleloWS_Service();
            serviciosweb.ParaleloWS port = service.getParaleloWSPort();
            String dparalelos = port.ingresarParaleloDocente(datosDocente, dparalelo);
            System.out.println("Nuevo paralelo. " + dparalelos);

            int auxint = Integer.parseInt(dparalelos);

            if (auxint >= 0) {
                docente objetoDocente = objetoGson.fromJson(datosDocente, docente.class);
                System.out.println(" Paralelo y lista del estudiante: " + dparalelos + " " + listaEst);
                paralelo opar = new paralelo();
                opar.setIntIdParalelo(auxint);
                opar.setIntIdPeriodo(objetoDocente.getIntPeriodo());
                String jsonParalelo = objetoGson.toJson(opar); //ESTE JSON + STRING LISTADO DE ESTUDIANTES

                serviciosweb.EstudianteWS_Service serviceR = new serviciosweb.EstudianteWS_Service();
                serviciosweb.EstudianteWS portE = serviceR.getEstudianteWSPort();
                String ListaESt = portE.ingresoEstudiantesParalelo(jsonParalelo, listaEst);

//                    response.setStatus(200);
//                    response.setContentType("text/plain");
//                    response.getWriter().write(ListaESt);
//                    response.sendRedirect("../Vistas/PanelDocenteV.jsp");
//                    response.sendRedirect("../Vistas/PanelDocenteV.jsp?ListaESt=" + ListaESt);
                if (ListaESt.equals("1")) {
                    System.out.println(" Lista ingresada con exito ");
                    response.sendRedirect("../Vistas/PanelDocenteV.jsp");
                } else {
                    System.out.println(" Lista no ingresada");
                    response.sendRedirect("../Vistas/PanelDocenteV.jsp");
                }
            }
        } else if (accion.equals("listadoParalelos")) {
            //Listar Paralelos 
            serviciosweb.ParaleloWS_Service service = new serviciosweb.ParaleloWS_Service();
            serviciosweb.ParaleloWS port = service.getParaleloWSPort();
            String datosParalelos = port.listarParalelosGeneral();
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(datosParalelos);
        } else if (accion.equals("listaEstudiantes")) {
            System.out.println("Listo para listar estudiantes");
            paralelo oParalelo = new paralelo();
            Gson oGson = new Gson();
            oParalelo.setIntIdParalelo(Integer.parseInt(opParal));
            String dParalelo = oGson.toJson(oParalelo);

            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String listaEstudiantes = port.listarEstudiantes(dParalelo);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(listaEstudiantes);
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  ParaleloM");
    }
%>