<%-- 
    Document   : PeriodoAca
    Created on : 25/11/2019, 16:42:44
    Author     : Angel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String opcion = request.getParameter("opcion"); //Data del periodo
    String dperiodo = request.getParameter("dperiodo"); //Data del periodo
    try {

        if (opcion.equals("ingresarPeriodoAca")) {

            serviciosweb.PeriodoacademicoWS_Service service = new serviciosweb.PeriodoacademicoWS_Service();
            serviciosweb.PeriodoacademicoWS port = service.getPeriodoacademicoWSPort();
            String periodoAca = port.ingresarPeriodoAcademico(dperiodo);
            
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(periodoAca);

        }
    } catch (Exception e) {
        System.out.println("Excepción Catch  PeriodoAcaM");
    }
%>
PeriodoAcaM