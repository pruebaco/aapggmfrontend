<%-- 
    Document   : ModeloParalelo
    Created on : 13/11/2019, 18:14:16
    Author     : Angel
--%>

<%@page import="ggm.comun.evaluacion"%>
<%@page import="ggm.comun.paralelo"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.docente"%>
<%@page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    paralelo oparalelo = new paralelo();
    docente oDocente = new docente();
    Gson oGson = new Gson();
    String datos = request.getParameter("datos");

    try {
        String opcion = request.getParameter("opcion"); //Opcion para ingresar materia
//        Mostrar el perfil del docente
        if (datos != null) {
//  Obtiene los datos del docente
            com.appggm.clientesWS.DocenteWS_Service service = new com.appggm.clientesWS.DocenteWS_Service();
            com.appggm.clientesWS.DocenteWS port = service.getDocenteWSPort();
            String datosD = port.perfilDocente(datos);
            oDocente = oGson.fromJson(datosD, docente.class);
            response.sendRedirect("../Controladores/DocenteC.jsp?datosD=" + datosD);
            System.out.println("Datos del docente " + datosD);
        } else if (opcion.equals("listarParalelosAct")) {
            System.out.println("Opcion modelos:  " + opcion);
            
            String datosDocente = session.getAttribute("datosDocente").toString();
            System.out.println(" recupéracion: " + datosDocente);

            com.appggm.clientesWS.DocenteWS_Service service = new com.appggm.clientesWS.DocenteWS_Service();
            com.appggm.clientesWS.DocenteWS port = service.getDocenteWSPort();
            String dClaseAct = port.listarClasesDocente(datosDocente);
            
            System.out.println(" espuesta: " + dClaseAct);
            
//creamos la session de la clase
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("dClaseAct", dClaseAct);

            response.sendRedirect("../Vistas/ActividadesV.jsp");
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  DocenteM");
    }
%>
