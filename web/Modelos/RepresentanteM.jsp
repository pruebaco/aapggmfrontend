<%-- 
    Document   : ModeloParalelo
    Created on : 13/11/2019, 18:14:16
    Author     : Angel
--%>

<%@page import="ggm.comun.estudiante"%>
<%@page import="ggm.comun.representante"%>
<%@page import="ggm.comun.evaluacion"%>
<%@page import="ggm.comun.paralelo"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.docente"%>
<%@page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String opcion = request.getParameter("opcion"); //Opciones

    Gson oGson = new Gson();
    representante oRepresentante = new representante();
    String datos = request.getParameter("datos");

    try {
        //        Mostrar el perfil del docente
        if (datos != null) {
//  Obtiene los datos del representante

            serviciosweb.RepresentanteWS_Service service = new serviciosweb.RepresentanteWS_Service();
            serviciosweb.RepresentanteWS port = service.getRepresentanteWSPort();
            String datosR = port.perfilRepresentante(datos);
            oRepresentante = oGson.fromJson(datosR, representante.class);

            response.sendRedirect("../Controladores/RepresentanteC.jsp?datosR=" + datosR);

        } else if (opcion.equals("modificarDatos")) {
            String dataModificarR = request.getParameter("dataModificarR");
            serviciosweb.RepresentanteWS_Service service = new serviciosweb.RepresentanteWS_Service();
            serviciosweb.RepresentanteWS port = service.getRepresentanteWSPort();
            String dataM = port.modificarRepresentante(dataModificarR);

            if (dataM.equals("1")) {

                String datosMod = port.perfilRepresentante(dataModificarR);
                oRepresentante = oGson.fromJson(datosMod, representante.class);
                String opcionAct = "datosActualizados";
                response.sendRedirect("../Controladores/RepresentanteOpC.jsp?datosMod=" + datosMod + "&opcion=" + opcionAct);
            }
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write("Exito");
            System.out.println("Respuesta:  " + dataM);
        } else if (opcion.equals("listaEstudiantes")) {
            //            recuperar la sesion del representante
            String datosRepresentante = session.getAttribute("datosRepresentante").toString();
            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String jsonEstudiantes = port.listadoEstudiantePorRepresentante(datosRepresentante);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(jsonEstudiantes);

        } else if (opcion.equals("listarNotasCualitativas")) {

            String opEstudiante = request.getParameter("opEstudiante");

            estudiante oEstudiante = new estudiante();
            oEstudiante.setStrCedula(opEstudiante);
            String jsonEstudiante = oGson.toJson(oEstudiante);

            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String dEstudiante = port.listarActividadesNotasEstudiantes(jsonEstudiante);

            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(dEstudiante);
        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  RepresentanteM");
    }
%>
