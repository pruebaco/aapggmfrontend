<%-- 
    Document   : EstudianteM
    Created on : 29/11/2019, 15:00:48
    Author     : Angel
--%>


<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.paralelo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    String opcion = request.getParameter("opcion"); //Opciones
    String opParal = request.getParameter("opParal"); //Opciones
    System.out.println("Id paralelo " + opParal);
    try {

        if (opcion.equals("listadoParalelos")) {

            serviciosweb.ParaleloWS_Service service = new serviciosweb.ParaleloWS_Service();
            serviciosweb.ParaleloWS port = service.getParaleloWSPort();
            String dParalelos = port.listarParalelosGeneral();

            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(dParalelos);

        } else if (opcion.equals("listaEstudiantes")) {
            
            paralelo oParalelo = new paralelo();
            Gson oGson = new Gson();
            oParalelo.setIntIdParalelo(Integer.parseInt(opParal));
            String dParalelo = oGson.toJson(oParalelo);

            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String listaParalelos = port.listarEstudiantes(dParalelo);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(listaParalelos);

        }
    } catch (Exception e) {
        System.out.println("Excepción Catch  ReporteM " + e.toString());
    }
%>