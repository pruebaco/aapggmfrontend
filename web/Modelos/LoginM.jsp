<%-- 
    Document   : LoginM
    Created on : 13/11/2019, 18:22:08
    Author     : Angel
--%>

<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.login"%>
<%@page session="true" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    login oLogin = new login();
    Gson oGson = new Gson();
    try {
//      Obtenemos cedula y contraseña desde el login del usuario y setear datos
        String ced = request.getParameter("cedula");
        String con = request.getParameter("contrasena");
        System.out.println("Cedula del usuario " + ced + " "+ con );

        oLogin.setStrCedula(ced);
        oLogin.setStrContrasena(con);
        String Credenciales = oGson.toJson(oLogin);

        if (!Credenciales.equals("")) {
            serviciosweb.SesionWS_Service service = new serviciosweb.SesionWS_Service();
            serviciosweb.SesionWS port1 = service.getSesionWSPort();
            String strEnvio = port1.verificarCuenta(Credenciales);
           
            response.sendRedirect("../Controladores/LoginC.jsp?datos=" + strEnvio);
            
        } else {
            System.out.println("Cuenta no verificada");
            response.sendRedirect("../Vistas/LoginV.jsp");
        }

    } catch (Exception e) {
        System.out.println("Exception catch: Cuenta no verificada");
    }
%>
