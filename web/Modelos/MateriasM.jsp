<%-- 
    Document   : PeriodoAca
    Created on : 25/11/2019, 16:42:44
    Author     : Angel
--%>

<%@page import="ggm.comun.docente"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="ggm.comun.clase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    String opcion = request.getParameter("opcion"); //Opciones para ingresar la materia
    System.out.println("Opciones de la materia : " + opcion);
//    String opcion = request.getParameter("opcion"); //Recibe datos de los paralelo

    String dMateria = request.getParameter("dMateria"); //Recibe datos de las materias
    String paraleloMat = request.getParameter("paraleloMat");
    System.out.println("Id de la materia : " + paraleloMat);
//    Separa el string de paralelo

    try {
        //listar materias
        if (opcion.equals("listarMateriasDoc")) {
            String datosDocente = session.getAttribute("datosDocente").toString(); //recuperamos la sesion del docente
            System.out.println(" recupéracion: " + datosDocente);

            com.appggm.clientesWS.DocenteWS_Service service = new com.appggm.clientesWS.DocenteWS_Service();
            com.appggm.clientesWS.DocenteWS port = service.getDocenteWSPort();
            String listarMaterias = port.listarMateriasDocente(datosDocente);
            System.out.println(" lista de materias  " + listarMaterias);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(listarMaterias);

        } else if (opcion.equals("listadoParalelosMat")) {
            serviciosweb.MateriaWS_Service service = new serviciosweb.MateriaWS_Service();
            serviciosweb.MateriaWS port = service.getMateriaWSPort();
            String paralelosMat = port.paralelosDeDocenteSinMateria();

            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(paralelosMat);

        } else if (opcion.equals("ingresarMateria")) {
            String[] arrayParalelo = paraleloMat.split(",");
            Gson objetoGson = new Gson();
            clase oclase = new clase();
            Gson oGson = new Gson();
//            docente oDocente= new docente();

            String datosDocente = session.getAttribute("datosDocente").toString();
            docente objetoDocente = objetoGson.fromJson(datosDocente, docente.class);

            oclase.setIntIdParalelo(Integer.parseInt(arrayParalelo[0]));
            oclase.setIntIdClase(Integer.parseInt(arrayParalelo[1]));
            oclase.setIntIdDocente(objetoDocente.getIntIdUsuario());

            String ObjClase = oGson.toJson(oclase);

//            Crea la session de la clase
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("dClase", ObjClase);

            System.out.println("JSON CLASE" + ObjClase);
            serviciosweb.MateriaWS_Service service = new serviciosweb.MateriaWS_Service();
            serviciosweb.MateriaWS port = service.getMateriaWSPort();
            String materiasDoc = port.ingresarMateria(ObjClase, dMateria);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(materiasDoc);

//            if (materiasDoc.equals("1")) {
//                response.setStatus(200);
//                response.setContentType("text/plain");
//                response.getWriter().write("Materia ingresada con Exito");
//
//            } else {
//                String error = "Se produjo un error";
//
//            }
        }
    } catch (Exception e) {
        System.out.println("Excepción Catch  MateriasM " + e.toString());
    }
%>