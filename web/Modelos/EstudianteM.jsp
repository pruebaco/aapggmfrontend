<%-- 
    Document   : EstudianteM
    Created on : 29/11/2019, 15:00:48
    Author     : Angel
--%>
<%@page import="ggm.comun.matricula"%>
<%@page import="ggm.comun.evaluacion"%>
<%@page import="ggm.comun.estudiante"%>
<%@page import="ggm.comun.parcial"%>
<%@page import="java.awt.List"%>
<%@page import="ggm.comun.docente"%>
<%@page import="ggm.comun.clase"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ggm.comun.paralelo"%>
<%@page import="com.google.gson.JsonPrimitive"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="ggm.comun.materia"%>
<%@page import="com.google.gson.Gson"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    String opcion = request.getParameter("opcion"); //Opciones

    String paraleloMat = request.getParameter("paraleloMat");
    String materiaEst = request.getParameter("materiaEst");
    String quimestreEst = request.getParameter("quimestreEst");
    String parcialEst = request.getParameter("parcialEst");

    try {

        if (opcion.equals("listarParalelosDoc")) {

            //recupera la session del docente
            String datosDocente = session.getAttribute("datosDocente").toString();
            System.out.println(" recupéracion: "+ datosDocente);

            com.appggm.clientesWS.DocenteWS_Service service = new com.appggm.clientesWS.DocenteWS_Service();
            com.appggm.clientesWS.DocenteWS port = service.getDocenteWSPort();
            String dClase = port.listarClasesDocente(datosDocente);

             HttpSession sesion = request.getSession(true);
            sesion.setAttribute("dClase", dClase);

            response.sendRedirect("../Vistas/EstudiantesV.jsp");

        } else if (opcion.equals("recuperarMaterias")) {
            int idParalelo = Integer.parseInt(request.getParameter("idParalelo"));
            String dClase = session.getAttribute("dClase").toString();
            JsonParser objetoM = new JsonParser();
            JsonElement objetoL = objetoM.parse(dClase); //Para convertir de string a Json
            JsonArray array = objetoL.getAsJsonArray();//Convertir de un elemento a un Array 
            Gson oGson = new Gson();
            ArrayList<materia> ObjMaterias = new ArrayList<materia>();
            for (int i = 0; i < array.size(); i++) {

                JsonElement objetoElemento = array.get(i); //Extrae uno x uno los elementos
                JsonObject objetoGeneral = objetoElemento.getAsJsonObject(); // El elemento json se convierte en objeto json
                materia objMateria = oGson.fromJson(objetoGeneral.get("materia").getAsString(), materia.class); //El objeto json en una clase materia
                clase objClase = oGson.fromJson(objetoGeneral.get("clase").getAsString(), clase.class); //El objeto json en una clase Clase
                if (objClase.getIntIdParalelo() == idParalelo) {
                    ObjMaterias.add(objMateria);
                }
            }
            response.setStatus(200);
            response.setContentType("text/plain");
            Gson oGSON = new Gson();
            response.getWriter().write(oGSON.toJson(ObjMaterias));

        } else if (opcion.equals("listarEstudiantes")) {
            System.out.println(" Entro a listarEstudiantes");
            clase objClase = new clase();
            Gson objetoGson = new Gson();
            parcial objParcial = new parcial();

            objClase.setIntIdClase(Integer.parseInt(materiaEst));
            String jsonClase = objetoGson.toJson(objClase);

            objParcial.setIntIdParcial(Integer.parseInt(parcialEst));
            objParcial.setIntIdQuimestre(Integer.parseInt(quimestreEst));
            String jsonParcial = objetoGson.toJson(objParcial);

            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String dEstudiantesPraelos = port.listarEstudiantesParalelo(jsonClase, jsonParcial);
//            //Deserializar inicio
            String json = "[";
            Gson oGson = new Gson();
            JsonParser objetoEstudiante = new JsonParser();
            JsonElement objetoEst = objetoEstudiante.parse(dEstudiantesPraelos); //Para convertir de string a Json
            JsonArray arrayEst = objetoEst.getAsJsonArray(); //Convertir de un elemento a un Array 

            for (int i = 0; i < arrayEst.size(); i++) {

                JsonElement objetoElemento = arrayEst.get(i); //Extrae uno x uno los elementos
                JsonObject objetoGeneral = objetoElemento.getAsJsonObject(); // El elemento json se convierte en objeto json
                estudiante objEstudiante = oGson.fromJson(objetoGeneral.get("estudiante").getAsString(), estudiante.class); //El objeto json en una clase estudiante
                evaluacion objEvaluacion = oGson.fromJson(objetoGeneral.get("evaluacion").getAsString(), evaluacion.class); //El objeto json en una clase evaluacion
                json += "{";
                json += "\"" + "cedula" + "\"" + ":" + "\"" + objEstudiante.getStrCedula() + "\"" + ",";
                json += "\"" + "nombre" + "\"" + ":" + "\"" + objEstudiante.getStrNombres() + "\"" + ",";
                json += "\"" + "apellido" + "\"" + ":" + "\"" + objEstudiante.getStrApellidos() + "\"" + ",";
                json += "\"" + "edad" + "\"" + ":" + "\"" + objEstudiante.getIntEdad() + "\"" + ",";
                json += "\"" + "idEvaluacion" + "\"" + ":" + "\"" + objEvaluacion.getIntIdEvaluacion() + "\"" + ",";
                json += "\"" + "nota1" + "\"" + ":" + "\"" + objEvaluacion.getIntNota1() + "\"" + ",";
                json += "\"" + "nota2" + "\"" + ":" + "\"" + objEvaluacion.getIntNota2() + "\"" + ",";
                json += "\"" + "nota3" + "\"" + ":" + "\"" + objEvaluacion.getIntNota3() + "\"" + ",";
                json += "\"" + "nota4" + "\"" + ":" + "\"" + objEvaluacion.getIntNota4() + "\"";
                json += "},";
            }
            json = json.substring(0, json.length() - 1);
            json += "]";
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(json);

        } else if (opcion.equals("guardarNotas")) {
            String notasEstudiante = request.getParameter("notasEstudiante");
            String idEvaluacion = request.getParameter("idEvaluacion");
            String idEstudiante = request.getParameter("idEstudiante");
            System.out.println("Id Evaluacion: " + idEvaluacion);
            Gson objetoGson = new Gson();
            clase objClase = new clase();
            matricula objMatricula = new matricula();
            evaluacion objEvaluacion = new evaluacion();
            parcial objParcial = new parcial();

            JsonParser objetoEst = new JsonParser();
            JsonElement objetoL = objetoEst.parse(notasEstudiante); //Para convertir de string a Json
            JsonArray array = objetoL.getAsJsonArray();//Convertir de un elemento a un Array 
            Gson oGson = new Gson();

            objEvaluacion.setIntIdEvaluacion(Integer.parseInt(idEvaluacion));
            objEvaluacion.setIntNota1(Integer.parseInt(array.get(0).toString()));
            objEvaluacion.setIntNota2(Integer.parseInt(array.get(1).toString()));
            objEvaluacion.setIntNota3(Integer.parseInt(array.get(2).toString()));
            objEvaluacion.setIntNota4(Integer.parseInt(array.get(3).toString()));
            String jsonEvaluacion = oGson.toJson(objEvaluacion);

            objMatricula.setIntIdMatricula(Integer.parseInt(idEstudiante));
            String jsonMatricula = objetoGson.toJson(objMatricula);
            objClase.setIntIdClase(Integer.parseInt(materiaEst));
            String jsonClase = objetoGson.toJson(objClase);

            objParcial.setIntIdParcial(Integer.parseInt(parcialEst));
            objParcial.setIntIdQuimestre(Integer.parseInt(quimestreEst));
            String jsonParcial = objetoGson.toJson(objParcial);

            serviciosweb.EstudianteWS_Service service = new serviciosweb.EstudianteWS_Service();
            serviciosweb.EstudianteWS port = service.getEstudianteWSPort();
            String dNotas = port.ingresarEvaluacion(jsonClase, jsonMatricula, jsonParcial, jsonEvaluacion);
            if (dNotas.equals("1")) {
                System.out.println("Exito ");
            } else {
                System.out.println("Error ");
            }

        }

    } catch (Exception e) {
        System.out.println("Excepción Catch  EstudiantesM " + e.toString());
    }
%>