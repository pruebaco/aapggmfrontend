<%-- 
    Document   : EstudianteM
    Created on : 29/11/2019, 15:00:48
    Author     : Angel
--%>

<%@page import="ggm.comun.actividad"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true" %>
<%
    String opcion = request.getParameter("opcion"); //Opciones

    try {

        if (opcion.equals("listarActividadesDoc")) {
            
            //recupera la session del docente
            String datosDocente = session.getAttribute("datosDocente").toString();
            
            com.appggm.clientesWS.DocenteWS_Service service = new com.appggm.clientesWS.DocenteWS_Service();
            com.appggm.clientesWS.DocenteWS port = service.getDocenteWSPort();
            String dActividades = port.listarActividadesFechaU(datosDocente);
            response.setStatus(200);
            response.setContentType("text/plain");
            response.getWriter().write(dActividades);

        }
    } catch (Exception e) {
        System.out.println("Excepción Catch  ActiviadesM " + e.toString());
    }
%>