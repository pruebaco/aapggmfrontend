<%-- 
    Document   : Representante
    Created on : 04/05/2019, 14:49:59
    Author     : Angel
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" type="text/css" href="css/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <title>Unidad educativa Dr. Gabriel García Moreno</title>
        <style>
            .carousel-control-prev {
                left: 20%;
                margin-top: 8%;
            }
            .carousel-control-next {
                right: 15%;
                margin-top: 8%;
            }

        </style>
    </head>
    <body>
        <div id="principal" class="main">

            <header >
                <div class="page-header" style="background:#252932; padding-left: 20px; padding-right: 20px" >
                    <div class="form-row">

                        <div class="col-md-2">
                            <img class="center-block" src="images/logo.jpg" width="55px" height="70px"
                                 style="margin-left: 40%; margin-top: 5%; margin-bottom: 3% ">
                        </div>
                        <div class="col-md-8">
                            <h1 style="width:100%; margin-left: 1%; margin-top:15px; color:#f2f2f2; "> Unidad Educativa "Dr. Gabriel García Moreno" </h1>
                        </div>
                        <div class="col-md-2">
                            <img class="center-block" src="images/logo2.jpg" width="55px" height="70px"
                                 style=" margin-top: 5%; margin-bottom: 3%; margin-left: 25%;">
                        </div>
                    </div>
                </div>
            </header>
            <!--Div de los botones--> 

            <div style="text-align: right; padding: 2%; ">
                <a href="Vistas/LoginV.jsp" type="button" class="btn btn-primary btn lg" > Login </a>
                <input  type="button" name="btn_registro" class="btn btn-primary" data-toggle="modal" value="Registro Usuario" id="btn_modalRegistro" data-target=".modalRegistro" onclick="listarPeriosdoAca();" return false>
                <!--<li><a onclick="listarActividades(); return false" href="#modalListarActividades" data-toggle="modal" data-target=".modal-listarActividades"> Activideades académicas </a></li>-->
            </div>


            <!--Carousel de Imagenes -->
            <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
                <!--Slides-->
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="center-block"  src="images/maestro.jpg" width="700px" height="350px"
                             alt="First slide" style="margin-top: 5%; margin-left: 25%;">
                    </div> 
                    <div class="carousel-item">
                        <img class="center-block" src="images/batalla.jpg" width="700px" height="350px"
                             alt="Second slide" style="margin-top: 5%; margin-left: 25%;">
                    </div>
                    <div class="carousel-item">
                        <img class="center-block" src="images/mensaje.jpg" width="700px" height="350px"
                             alt="Third slide" style="margin-top: 5%; margin-left: 25%;">
                    </div>
                </div>
                <!--/.Slides-->

                <!--Controls-->
                <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev" >
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carousel-thumb" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <!--/.Controls-->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-thumb" data-slide-to="0" class="active">
                        <img src="images/maestro.jpg" width="100px" height="50px">
                    </li>
                    <li data-target="#carousel-thumb" data-slide-to="1">
                        <img src="images/batalla.jpg" width="100px" height="50px">
                    </li>
                    <li data-target="#carousel-thumb" data-slide-to="2">
                        <img src="images/mensaje.jpg" width="100px" height="50px">
                    </li>
                </ol>

            </div>


            <!--Modal para registar Usuarios--> 
            <div class="modal fade modalRegistro" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header" style="background: #004050; color: white;">
                            <h5 class="modal-title" id="exampleModalLabel" style="text-align: center;" >REGISTRO DE USUARIOS</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <br>
                            <!--<form  method="get" enctype="text/plain" class="needs-validation" novalidate>-->
                            <form  method="get" enctype="text/plain" >

                                <div class="form-row">
                                    <div class="col-md-5 mb-4">
                                        <label for="validationCustom01">Nombres</label>
                                        <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombres" value="Luis" required>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <label for="validationCustom02">Apellidos</label>
                                        <input type="text" class="form-control" id="txtApellido" name="txtApellido" placeholder="Apellidos" value="Ortiz" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-5">
                                        <label for="inputState">Periodo académico</label>
                                        <select name="peridoAca" id="opPeriodoAca" class="custom-select" required> 
                                            <option value="">Abrir el menú para seleccionar</option>
                                        </select>
                                        <div class="invalid-feedback">Seleccione el periodo académico</div>
                                    </div>

                                    <div class="col-md-4 mb-3">
                                        <label for="validationCustom02">Cédula</label>
                                        <input type="text" class="form-control" id="txtCedula" name="txtCedula"  placeholder="110434423-0" required>
                                        <div class="invalid-feedback">Ingrese la cédula </div>
                                    </div>

                                    <div class="form-group col-md-2 mb-4">
                                        <label for="inputState">Edad</label>
                                        <input type="number" class="form-control"id="txtEdad" name="txtEdad"  placeholder="Edad" min="18"  max="60" required>
                                        <div class="invalid-feedback">Ingrese la edad</div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-5 mb-4">
                                        <label for="exampleInputEmail1">Correo</label>
                                        <input type="email" class="form-control" id="txtCorreo" name="txtCorreo" aria-describedby="emailHelp" placeholder="Ingrese correo electrónico" required>
                                        <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
                                        <div class="invalid-feedback">Ingrese una dirección de correo válida</div>
                                    </div>
                                    <div class="form-group col-md-6 mb-4">
                                        <label for="exampleInputPassword1">Contraseña</label>
                                        <input type="password" class="form-control" id="txtContrasena" name="txtContrasena" placeholder="Password" required>
                                        <div class="invalid-feedback">Ingrese una contraseña</div>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 mb-2">
                                        <label >Tipo </label>
                                    </div>
                                    <div class="custom-control custom-radio col-md-3 mb-4">
                                        <input type="radio" id="opTipo" name="opTipo" value="Docente" required>
                                        <label > Docente</label>

                                    </div>
                                    <div class="custom-control custom-radio col-md-3 mb-4">
                                        <input type="radio" id="opTipo" name="opTipo" value="Representante" required>
                                        <label  > Representante</label>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-3 mb-4">
                                        <label >Telefono</label>
                                        <input type="text" class="form-control" id="txtTelefono" name="txtTelefono" placeholder="(3)900-XXXX"  >

                                    </div>
                                    <div class="col-md-3 mb-4">
                                        <label for="validationCustom02">Celular 1</label>
                                        <input type="text" class="form-control" id="txtCelular1" name="txtCelular1" placeholder="0983878499"  required>
                                        <div class="invalid-feedback">Ingrese un número</div>
                                    </div>
                                    <div class="col-md-3 mb-4">
                                        <label >Celular 2</label>
                                        <input type="text" class="form-control" id="txtCelular2" name="txtCelular2" placeholder="0983878445"  >

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <input type="button" id="btn_guardar" name="btn_guardar"  class="btn btn-primary" value="Guardar" onclick="registroUsuario(); return false">
                                </div>
                            </form>
                        </div>

                        <script>
                            // Example starter JavaScript for disabling form submissions if there are invalid fields
                            (function () {
                                'use strict';
                                window.addEventListener('load', function () {
                                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                    var forms = document.getElementsByClassName('needs-validation');
                                    // Loop over them and prevent submission
                                    var validation = Array.prototype.filter.call(forms, function (form) {
                                        form.addEventListener('submit', function (event) {
                                            if (form.checkValidity() === false) {
                                                event.preventDefault();
                                                event.stopPropagation();
                                            }
                                            form.classList.add('was-validated');
                                        }, false);
                                    });
                                }, false);
                            })();
                        </script>

                    </div>
                </div>
            </div>
            <!--Fin de la Modal para registar Usuarios--> 
        </div>
        <script type="text/javascript">
//            listar periodo academico
            function listarPeriosdoAca() {
                $.ajax({
                    url: "Modelos/RegistroM.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: 'listadoPeriodosAca'
                    },
                    success: function (resultado) {
                        var periodos = JSON.parse(resultado);
                        var html = "";
                        periodos.forEach(function (periodo) {
                            html += "<option value='" + periodo.intIdPeriodo + "'>" + periodo.strPeriodoAcademico + "</option>";
                        });
                        $('#opPeriodoAca').html(html);
                        $('#modalRegistro').modal('show');

                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }



//            Registrar usuario
            function registroUsuario() {
                var nombreUsu = document.getElementById('txtNombre').value;
                var apellidoUsu = document.getElementById('txtApellido').value;
                var peridoAcad = document.getElementById('opPeriodoAca').value;
                var cedulaUsu = document.getElementById('txtCedula').value;
                var edadUsu = document.getElementById('txtEdad').value;
                var correoUsu = document.getElementById('txtCorreo').value;
                var contrasenaUsu = document.getElementById('txtContrasena').value;
                var tipoUsu = document.getElementById('opTipo').value;
                var telefonoUsu = document.getElementById('txtTelefono').value;
                var celular1Usu = document.getElementById('txtCelular1').value;
                var celular2Usu = document.getElementById('txtCelular2').value;

                $.ajax({
                    url: "Controladores/RegistroC.jsp",
                    type: 'GET',
                    dataType: 'text',
                    data: {
                        opcion: "registrarUsuarios",
                        nombreU: nombreUsu,
                        apellidoU: apellidoUsu,
                        perioAcaU: peridoAcad,
                        cedulaU: cedulaUsu,
                        edadU: edadUsu,
                        correoU: correoUsu,
                        contrasenaU: contrasenaUsu,
                        tipoU: tipoUsu,
                        telefonoU: telefonoUsu,
                        celular1U: celular1Usu,
                        celular2U: celular2Usu,
                    },
                    success: function (response) {
//                        document.getElementById('txtNombre').value = '';

                        $('#modalRegistro').modal('hide');
                        Swal.fire({
                            icon: 'success',
                            title: 'Éxito',
                            text: response
                        })
                        document.getElementById('txtNombre').value = '';
                        document.getElementById('txtApellido').value = '';
                        document.getElementById('opPeriodoAca').value = '';
                        document.getElementById('txtCedula').value = '';
                        document.getElementById('txtEdad').value = '';
                        document.getElementById('txtCorreo').value = '';
                        document.getElementById('txtContrasena').value = '';
                        document.getElementById('opTipo').value = '';
                        document.getElementById('txtTelefono').value = '';
                        document.getElementById('txtCelular1').value = '';
                        document.getElementById('txtCelular2').value = '';
                    },
                    error: function (error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: error
                        })
                    }
                });

            }

        </script>
        <!--opciones js-->

        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/sweetalert2.all.min.js"></script>
        <!--<script src="docente.js"></script>-->

    </body>
</html>

